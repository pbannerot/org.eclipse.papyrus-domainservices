/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import static org.junit.jupiter.api.Assertions.assertFalse;

import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.FinalState;
import org.eclipse.uml2.uml.UMLPackage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ElementCreatorCheckerTest extends AbstractUMLTest {

    private ElementCreationChecker elementCreatorChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        elementCreatorChecker = new ElementCreationChecker();
    }

    @Test
    public void testCreateRegionInFinalState() {
        FinalState finalState = create(FinalState.class);

        CheckStatus canCreateStatus = elementCreatorChecker.canCreate(finalState,
                UMLPackage.eINSTANCE.getRegion().getName(), "region");
        assertFalse(canCreateStatus.isValid());
    }

}
