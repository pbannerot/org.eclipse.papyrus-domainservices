/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.destroy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * This class tests the deletion of StateMachine elements.
 * 
 * @author lfasani
 *
 */
public class StateMachineElementsDestroyerTest extends AbstractUMLTest {

    private static final String T_1_FINAL = "T_1_final";
    private static final String T_INIT_1 = "T_init_1";
    private static final String REGION = "region";
    private static final String STATE1 = "state1";
    private IDestroyer destroyer;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        destroyer = ElementDestroyer.buildDefault(getCrossRef(), this.getEditableChecker());
    }

    /**
     * Check the deletion of a given {@link Region} from a {@link StateMachine}.
     */
    @Test
    public void deleteRegionTest() {
        Model model = create(Model.class);
        StateMachine stateMachine = create(StateMachine.class);
        model.getPackagedElements().add(stateMachine);
        Region region = create(Region.class);
        Region region2 = create(Region.class);
        stateMachine.getRegions().addAll(List.of(region, region2));

        DestroyerStatus destroyStatus = destroyer.destroy(region);
        assertEquals(org.eclipse.papyrus.uml.domain.services.status.State.DONE, destroyStatus.getState());
        assertEquals(1, stateMachine.getRegions().size());
        assertFalse(stateMachine.getRegions().contains(region));

        destroyStatus = destroyer.destroy(region2);
        // the last region of a StateMachine can not be destroyed
        assertEquals(org.eclipse.papyrus.uml.domain.services.status.State.FAILED, destroyStatus.getState());
        assertEquals(1, stateMachine.getRegions().size());
        assertTrue(stateMachine.getRegions().contains(region2));

        destroyStatus = destroyer.destroy(stateMachine);
        assertEquals(org.eclipse.papyrus.uml.domain.services.status.State.DONE, destroyStatus.getState());
        assertTrue(model.getOwnedElements().isEmpty());
    }

    /**
     * Check the deletion of a given {@link State} from a {@link Region}.
     */
    @Test
    public void deleteStateTest() {
        StateMachine stateMachine = initStateMachine();
        Optional<State> state1 = getElement(stateMachine, State.class, STATE1);

        destroyer.destroy(state1.get());
        state1 = getElement(stateMachine, State.class, STATE1);
        assertFalse(state1.isPresent());

        Optional<Region> region = getElement(stateMachine, Region.class, REGION);
        assertNull(region.get().getTransition(T_INIT_1));
        assertNull(region.get().getTransition(T_1_FINAL));
    }

    private StateMachine initStateMachine() {
        StateMachine stateMachine = create(StateMachine.class);
        Region region = createNamedElement(Region.class, REGION);
        stateMachine.getRegions().add(region);

        State initialState = createNamedElement(State.class, "initialState");
        State state1 = createNamedElement(State.class, STATE1);
        State finalState = createNamedElement(State.class, "finalState");
        region.getSubvertices().addAll(List.of(initialState, state1, finalState));

        Transition transition1 = createNamedElement(Transition.class, T_INIT_1);
        transition1.setSource(initialState);
        transition1.setTarget(state1);
        Transition transition2 = createNamedElement(Transition.class, T_1_FINAL);
        transition2.setSource(state1);
        transition2.setTarget(finalState);
        region.getTransitions().addAll(List.of(transition1, transition2));

        return stateMachine;
    }
}
