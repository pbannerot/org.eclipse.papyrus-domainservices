/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.ActivityInternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.AcceptCallAction;
import org.eclipse.uml2.uml.AcceptEventAction;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.CallBehaviorAction;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.ControlFlow;
import org.eclipse.uml2.uml.InitialNode;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.InterruptibleActivityRegion;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.OpaqueAction;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.StructuredActivityNode;
import org.junit.jupiter.api.Test;

/**
 * Test class for
 * {@link ActivityInternalSourceToRepresentationDropBehaviorProvider}.
 *
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 *
 */
public class ActivityInternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    /**
     * Test dropping an ActivityNode from an ActivityPartition to an Activity, the
     * ActivityPartition is contained by this Activity : The Activity container
     * should not changed, ActivityPartition#node features should be updated.
     */
    @Test
    public void testActivityNodeDropFromActivityPartitionToActivity() {
        Activity root = this.create(Activity.class);
        ActivityPartition activityPartition1 = this.create(ActivityPartition.class);
        root.getOwnedGroups().add(activityPartition1);
        root.getPartitions().add(activityPartition1);
        InitialNode nodeToDrop = this.create(InitialNode.class);
        nodeToDrop.setActivity(root);
        nodeToDrop.getInPartitions().add(activityPartition1);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop,
                activityPartition1, root, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(root.getOwnedNodes().contains(nodeToDrop));
        assertFalse(activityPartition1.getNodes().contains(nodeToDrop));
    }

    /**
     * Test dropping an ActivityNode from an ActivityPartition to another
     * ActivityPartition contained by a different Activity: The Activity container
     * should changed, ActivityPartition#node feature should be updated.
     */
    @Test
    public void testActivityNodeDropFromActivityPartitionToActivityPartitionWithDifferentActivityContainer() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        Activity subActivity2 = this.createIn(Activity.class, root);
        ActivityPartition activityPartition1 = this.create(ActivityPartition.class);
        ActivityPartition activityPartition2 = this.create(ActivityPartition.class);
        subActivity1.getOwnedGroups().add(activityPartition1);
        subActivity1.getPartitions().add(activityPartition1);
        subActivity2.getOwnedGroups().add(activityPartition2);
        subActivity2.getPartitions().add(activityPartition2);
        CallBehaviorAction callBehaviorActionToDrop = this.create(CallBehaviorAction.class);
        callBehaviorActionToDrop.setActivity(subActivity1);
        callBehaviorActionToDrop.getInPartitions().add(activityPartition1);
        InputPin inputPin1 = this.createIn(InputPin.class, callBehaviorActionToDrop);
        OutputPin outputPin1 = this.createIn(OutputPin.class, callBehaviorActionToDrop);
        inputPin1.getInPartitions().add(activityPartition1);
        outputPin1.getInPartitions().add(activityPartition1);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(callBehaviorActionToDrop,
                activityPartition1, activityPartition2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertFalse(activityPartition1.getNodes().contains(callBehaviorActionToDrop));
        assertFalse(activityPartition1.getNodes().contains(inputPin1));
        assertFalse(activityPartition1.getNodes().contains(outputPin1));
        assertFalse(subActivity1.getOwnedNodes().contains(callBehaviorActionToDrop));
        assertTrue(activityPartition2.getNodes().contains(callBehaviorActionToDrop));
        assertTrue(activityPartition2.getNodes().contains(inputPin1));
        assertTrue(activityPartition2.getNodes().contains(outputPin1));
        assertTrue(subActivity2.getOwnedNodes().contains(callBehaviorActionToDrop));
    }

    /**
     * Test dropping an ActivityNode from an ActivityPartition to another
     * ActivityPartition contained by the same Activity: The Activity container
     * should not changed, ActivityPartition#node feature should be updated.
     */
    @Test
    public void testActivityNodeDropFromActivityPartitionToActivityPartitionWithSameActivityContainer() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        ActivityPartition activityPartition1 = this.create(ActivityPartition.class);
        ActivityPartition activityPartition2 = this.create(ActivityPartition.class);
        subActivity1.getOwnedGroups().add(activityPartition1);
        subActivity1.getPartitions().add(activityPartition1);
        subActivity1.getOwnedGroups().add(activityPartition2);
        subActivity1.getPartitions().add(activityPartition2);
        CallBehaviorAction callBehaviorActionToDrop = this.create(CallBehaviorAction.class);
        callBehaviorActionToDrop.setActivity(subActivity1);
        callBehaviorActionToDrop.getInPartitions().add(activityPartition1);
        InputPin inputPin1 = this.createIn(InputPin.class, callBehaviorActionToDrop);
        OutputPin outputPin1 = this.createIn(OutputPin.class, callBehaviorActionToDrop);
        inputPin1.getInPartitions().add(activityPartition1);
        outputPin1.getInPartitions().add(activityPartition1);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(callBehaviorActionToDrop,
                activityPartition1, activityPartition2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertFalse(activityPartition1.getNodes().contains(callBehaviorActionToDrop));
        assertFalse(activityPartition1.getNodes().contains(inputPin1));
        assertFalse(activityPartition1.getNodes().contains(outputPin1));
        assertTrue(subActivity1.getOwnedNodes().contains(callBehaviorActionToDrop));
        assertTrue(activityPartition2.getNodes().contains(callBehaviorActionToDrop));
        assertTrue(activityPartition2.getNodes().contains(inputPin1));
        assertTrue(activityPartition2.getNodes().contains(outputPin1));
    }

    /**
     * Test dropping an ActivityNode from an ActivityPartition to an
     * InterruptibleActivityRegion contained by a different Activity: The Activity
     * container should changed, InterruptibleActivityRegion#node and
     * ActivityPartition#node features should be updated.
     */
    @Test
    public void testActivityNodeDropFromActivityPartitionToInterruptibleActivityRegionWithDifferentActivityContainer() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        Activity subActivity2 = this.createIn(Activity.class, root);
        ActivityPartition activityPartition1 = this.create(ActivityPartition.class);
        InterruptibleActivityRegion interruptibleActivityRegion2 = this.create(InterruptibleActivityRegion.class);
        subActivity1.getOwnedGroups().add(activityPartition1);
        subActivity1.getPartitions().add(activityPartition1);
        subActivity2.getOwnedGroups().add(interruptibleActivityRegion2);
        InitialNode nodeToDrop = this.create(InitialNode.class);
        nodeToDrop.setActivity(subActivity1);
        nodeToDrop.getInPartitions().add(activityPartition1);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop,
                activityPartition1, interruptibleActivityRegion2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertFalse(activityPartition1.getNodes().contains(nodeToDrop));
        assertFalse(subActivity1.getOwnedNodes().contains(nodeToDrop));
        assertTrue(interruptibleActivityRegion2.getNodes().contains(nodeToDrop));
        assertTrue(subActivity2.getOwnedNodes().contains(nodeToDrop));
    }

    /**
     * Test dropping an ActivityNode from an ActivityPartition to an
     * InterruptibleActivityRegion contained by the same Activity: The Activity
     * container should not changed, InterruptibleActivityRegion#node and
     * ActivityPartition#node features should be updated.
     */
    @Test
    public void testActivityNodeDropFromActivityPartitionToInterruptibleActivityRegionWithSameActivityContainer() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        ActivityPartition activityPartition1 = this.create(ActivityPartition.class);
        InterruptibleActivityRegion interruptibleActivityRegion2 = this.create(InterruptibleActivityRegion.class);
        subActivity1.getOwnedGroups().add(activityPartition1);
        subActivity1.getPartitions().add(activityPartition1);
        subActivity1.getOwnedGroups().add(interruptibleActivityRegion2);
        InitialNode nodeToDrop = this.create(InitialNode.class);
        nodeToDrop.setActivity(subActivity1);
        nodeToDrop.getInPartitions().add(activityPartition1);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop,
                activityPartition1, interruptibleActivityRegion2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertFalse(activityPartition1.getNodes().contains(nodeToDrop));
        assertTrue(subActivity1.getOwnedNodes().contains(nodeToDrop));
        assertTrue(interruptibleActivityRegion2.getNodes().contains(nodeToDrop));
    }

    /**
     * Test dropping an ActivityNode from an Activity to another Activity: The
     * Activity container should changed.
     */
    @Test
    public void testActivityNodeDropFromActivityToActivity() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        Activity subActivity2 = this.createIn(Activity.class, root);
        InitialNode nodeToDrop = this.create(InitialNode.class);
        nodeToDrop.setActivity(subActivity1);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop, subActivity1,
                subActivity2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertFalse(subActivity1.getOwnedNodes().contains(nodeToDrop));
        assertTrue(subActivity2.getOwnedNodes().contains(nodeToDrop));
    }

    /**
     * Test dropping an ActivityNode from an Activity to an ActivityPartition
     * contained by this Activity: The Activity container should not changed,
     * ActivityPartition#node features should be updated.
     */
    @Test
    public void testActivityNodeDropFromActivityToActivityPartition() {
        Activity root = this.create(Activity.class);
        ActivityPartition activityPartition1 = this.create(ActivityPartition.class);
        root.getOwnedGroups().add(activityPartition1);
        root.getPartitions().add(activityPartition1);
        InitialNode nodeToDrop = this.create(InitialNode.class);
        nodeToDrop.setActivity(root);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop, root,
                activityPartition1, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(root.getOwnedNodes().contains(nodeToDrop));
        assertTrue(activityPartition1.getNodes().contains(nodeToDrop));
    }

    /**
     * Test dropping an ActivityNode from an InterruptibleActivityRegion to an
     * ActivityPartition contained by a different Activity: The Activity container
     * should changed, InterruptibleActivityRegion#node and ActivityPartition#node
     * features should be updated.
     */
    @Test
    public void testActivityNodeDropFromInterruptibleActivityRegionToActivityPartitionWithDifferentActivityContainer() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        Activity subActivity2 = this.createIn(Activity.class, root);
        InterruptibleActivityRegion interruptibleActivityRegion1 = this.create(InterruptibleActivityRegion.class);
        ActivityPartition activityPartition2 = this.create(ActivityPartition.class);
        subActivity1.getOwnedGroups().add(interruptibleActivityRegion1);
        subActivity2.getOwnedGroups().add(activityPartition2);
        subActivity2.getPartitions().add(activityPartition2);
        InitialNode nodeToDrop = this.create(InitialNode.class);
        nodeToDrop.setActivity(subActivity1);
        nodeToDrop.getInInterruptibleRegions().add(interruptibleActivityRegion1);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop,
                interruptibleActivityRegion1, activityPartition2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertFalse(interruptibleActivityRegion1.getNodes().contains(nodeToDrop));
        assertFalse(subActivity1.getOwnedNodes().contains(nodeToDrop));
        assertTrue(activityPartition2.getNodes().contains(nodeToDrop));
        assertTrue(subActivity2.getOwnedNodes().contains(nodeToDrop));
    }

    /**
     * Test dropping an ActivityNode from an InterruptibleActivityRegion to an
     * ActivityPartition contained by the same Activity: The Activity container
     * should not changed, InterruptibleActivityRegion#node and
     * ActivityPartition#node features should be updated.
     */
    @Test
    public void testActivityNodeDropFromInterruptibleActivityRegionToActivityPartitionWithSameActivityContainer() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        InterruptibleActivityRegion interruptibleActivityRegion1 = this.create(InterruptibleActivityRegion.class);
        ActivityPartition activityPartition2 = this.create(ActivityPartition.class);
        subActivity1.getOwnedGroups().add(interruptibleActivityRegion1);
        subActivity1.getOwnedGroups().add(activityPartition2);
        subActivity1.getPartitions().add(activityPartition2);
        InitialNode nodeToDrop = this.create(InitialNode.class);
        nodeToDrop.setActivity(subActivity1);
        nodeToDrop.getInInterruptibleRegions().add(interruptibleActivityRegion1);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop,
                interruptibleActivityRegion1, activityPartition2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertFalse(interruptibleActivityRegion1.getNodes().contains(nodeToDrop));
        assertTrue(subActivity1.getOwnedNodes().contains(nodeToDrop));
        assertTrue(activityPartition2.getNodes().contains(nodeToDrop));
    }

    /**
     * Test dropping an ActivityNode from an InterruptibleActivityRegion to another
     * InterruptibleActivityRegion contained by a different Activity: The Activity
     * container should changed, InterruptibleActivityRegion#node feature should be
     * updated.
     */
    @Test
    public void testActivityNodeDropFromInterruptibleActivityRegionToInterruptibleActivityRegionWithDifferentActivityContainer() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        Activity subActivity2 = this.createIn(Activity.class, root);
        InterruptibleActivityRegion interruptibleActivityRegion1 = this.create(InterruptibleActivityRegion.class);
        InterruptibleActivityRegion interruptibleActivityRegion2 = this.create(InterruptibleActivityRegion.class);
        subActivity1.getOwnedGroups().add(interruptibleActivityRegion1);
        subActivity2.getOwnedGroups().add(interruptibleActivityRegion2);
        InitialNode nodeToDrop = this.create(InitialNode.class);
        nodeToDrop.setActivity(subActivity1);
        nodeToDrop.getInInterruptibleRegions().add(interruptibleActivityRegion1);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop,
                interruptibleActivityRegion1, interruptibleActivityRegion2, this.getCrossRef(),
                this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertFalse(interruptibleActivityRegion1.getNodes().contains(nodeToDrop));
        assertFalse(subActivity1.getOwnedNodes().contains(nodeToDrop));
        assertTrue(interruptibleActivityRegion2.getNodes().contains(nodeToDrop));
        assertTrue(subActivity2.getOwnedNodes().contains(nodeToDrop));
    }

    /**
     * Test dropping an ActivityNode from an InterruptibleActivityRegion to another
     * InterruptibleActivityRegion contained by the same Activity: The Activity
     * container should not changed, InterruptibleActivityRegion#node feature should
     * be updated.
     */
    @Test
    public void testActivityNodeDropFromInterruptibleActivityRegionToInterruptibleActivityRegionWithSameActivityContainer() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        InterruptibleActivityRegion interruptibleActivityRegion1 = this.create(InterruptibleActivityRegion.class);
        InterruptibleActivityRegion interruptibleActivityRegion2 = this.create(InterruptibleActivityRegion.class);
        subActivity1.getOwnedGroups().add(interruptibleActivityRegion1);
        subActivity1.getOwnedGroups().add(interruptibleActivityRegion2);
        InitialNode nodeToDrop = this.create(InitialNode.class);
        nodeToDrop.setActivity(subActivity1);
        nodeToDrop.getInInterruptibleRegions().add(interruptibleActivityRegion1);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop,
                interruptibleActivityRegion1, interruptibleActivityRegion2, this.getCrossRef(),
                this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertFalse(interruptibleActivityRegion1.getNodes().contains(nodeToDrop));
        assertTrue(subActivity1.getOwnedNodes().contains(nodeToDrop));
        assertTrue(interruptibleActivityRegion2.getNodes().contains(nodeToDrop));
    }

    /**
     * Test dropping an ActivityNode from a StructuredActivityNode to another
     * StructuredActivityNode: The StructuredActivityNode container should changed.
     */
    @Test
    public void testActivityNodeDropFromStructuredActivityNodeToStructuredActivityNode() {
        Activity root = this.create(Activity.class);
        StructuredActivityNode structuredNode1 = this.createIn(StructuredActivityNode.class, root);
        StructuredActivityNode structuredNode2 = this.createIn(StructuredActivityNode.class, root);
        InitialNode nodeToDrop = this.create(InitialNode.class);
        nodeToDrop.setInStructuredNode(structuredNode1);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop,
                structuredNode1, structuredNode2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertFalse(structuredNode1.getNodes().contains(nodeToDrop));
        assertTrue(structuredNode2.getNodes().contains(nodeToDrop));
    }

    @Test
    public void testActivityNodeWithActivityEdgeDropFromStructuredActivityNodeToStructuredActivityNode() {
        Activity root = this.create(Activity.class);
        StructuredActivityNode structuredActivityNode = this.createIn(StructuredActivityNode.class, root);
        StructuredActivityNode subStructuredActivityNode1 = this.createIn(StructuredActivityNode.class,
                structuredActivityNode);
        StructuredActivityNode subStructuredActivityNode2 = this.createIn(StructuredActivityNode.class,
                structuredActivityNode);

        AcceptCallAction nodeToDrop = this.createIn(AcceptCallAction.class, subStructuredActivityNode1);
        AcceptCallAction acceptCallAction1 = this.createIn(AcceptCallAction.class, subStructuredActivityNode1);
        ControlFlow activityEdge = this.createIn(ControlFlow.class, subStructuredActivityNode1);
        activityEdge.setSource(nodeToDrop);
        activityEdge.setTarget(acceptCallAction1);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop,
                subStructuredActivityNode1, subStructuredActivityNode2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertEquals(structuredActivityNode, activityEdge.getOwner());
    }

    /**
     * Test dropping an ActivityNode linked by an ActivityEdge in another Activity:
     * the container of the ActivityEdge should changed.
     */
    @Test
    public void testActivityNodeWithActivityEdgeDropToActivity() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        Activity subActivity2 = this.createIn(Activity.class, root);
        AcceptCallAction nodeToDrop = this.createIn(AcceptCallAction.class, subActivity1);
        AcceptCallAction acceptCallAction1 = this.createIn(AcceptCallAction.class, subActivity1);
        ControlFlow activityEdge = this.createIn(ControlFlow.class, subActivity1);
        activityEdge.setSource(nodeToDrop);
        activityEdge.setTarget(acceptCallAction1);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop, subActivity1,
                subActivity2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertFalse(subActivity1.getOwnedNodes().contains(nodeToDrop));
        assertTrue(subActivity2.getOwnedNodes().contains(nodeToDrop));
        assertEquals(subActivity2, activityEdge.getOwner());
    }

    /**
     * Test dropping an ActivityNode linked by an ActivityEdge in another
     * StructuredActivityNode: the container of the ActivityEdge should changed.
     */
    @Test
    public void testActivityNodeWithActivityEdgeDropToStructuredActivityNode() {
        Activity root = this.create(Activity.class);
        StructuredActivityNode structuredActivityNode1 = this.createIn(StructuredActivityNode.class, root);
        StructuredActivityNode structuredActivityNode2 = this.createIn(StructuredActivityNode.class, root);
        AcceptCallAction nodeToDrop = this.createIn(AcceptCallAction.class, structuredActivityNode1);
        AcceptCallAction acceptCallAction1 = this.createIn(AcceptCallAction.class, structuredActivityNode1);
        ControlFlow activityEdge = this.createIn(ControlFlow.class, structuredActivityNode1);
        activityEdge.setSource(nodeToDrop);
        activityEdge.setTarget(acceptCallAction1);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop,
                structuredActivityNode1, structuredActivityNode2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertEquals(root, activityEdge.getOwner());
    }

    @Test
    public void testActivityNodeWithActivityEdgeInPartitionDropFromActivityPartitionToActivityPartition() {
        Activity root = this.create(Activity.class);
        ActivityPartition partition1 = this.createIn(ActivityPartition.class, root);
        ActivityPartition partition2 = this.createIn(ActivityPartition.class, root);
        AcceptCallAction nodeToDrop = this.createIn(AcceptCallAction.class, root);
        nodeToDrop.getInPartitions().add(partition1);
        AcceptCallAction acceptCallAction1 = this.createIn(AcceptCallAction.class, root);
        acceptCallAction1.getInPartitions().add(partition1);
        ControlFlow activityEdge = this.createIn(ControlFlow.class, root);
        activityEdge.getInPartitions().add(partition1);
        activityEdge.setSource(nodeToDrop);
        activityEdge.setTarget(acceptCallAction1);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop, partition1,
                partition2, this.getCrossRef(), this.getEditableChecker());
        // We need to DnD the source and target of the ActivityEdge to change its
        // inPartition feature.
        status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(acceptCallAction1, partition1,
                partition2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertEquals(root, activityEdge.getOwner());
        assertEquals(partition2, activityEdge.getInPartitions().get(0));
    }

    /**
     * Test dropping an OpaqueAction linked by an ActivityEdge through Pin: the
     * container of the ActivityEdge should changed.
     */
    @Test
    public void testActivityNodeWithObjectFlowSourceDropToStructuredActivityNode() {
        Activity root = this.create(Activity.class);
        StructuredActivityNode structuredActivityNode1 = this.createIn(StructuredActivityNode.class, root);
        StructuredActivityNode structuredActivityNode2 = this.createIn(StructuredActivityNode.class, root);
        OpaqueAction nodeToDrop = this.createIn(OpaqueAction.class, structuredActivityNode1);
        OpaqueAction opaqueAction1 = this.createIn(OpaqueAction.class, structuredActivityNode1);
        ObjectFlow activityEdge = this.createIn(ObjectFlow.class, structuredActivityNode1);
        OutputPin outputPin = this.create(OutputPin.class);
        nodeToDrop.getOutputValues().add(outputPin);
        activityEdge.setSource(outputPin);
        InputPin inputPin = this.create(InputPin.class);
        opaqueAction1.getInputValues().add(inputPin);
        activityEdge.setTarget(inputPin);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop,
                structuredActivityNode1, structuredActivityNode2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertEquals(root, activityEdge.getOwner());
    }

    /**
     * Test dropping an OpaqueAction linked by an ActivityEdge through Pin: the
     * container of the ActivityEdge should changed.
     */
    @Test
    public void testActivityNodeWithObjectFlowTargetDropToStructuredActivityNode() {
        Activity root = this.create(Activity.class);
        StructuredActivityNode structuredActivityNode1 = this.createIn(StructuredActivityNode.class, root);
        StructuredActivityNode structuredActivityNode2 = this.createIn(StructuredActivityNode.class, root);
        OpaqueAction nodeToDrop = this.createIn(OpaqueAction.class, structuredActivityNode1);
        OpaqueAction opaqueAction1 = this.createIn(OpaqueAction.class, structuredActivityNode1);
        ObjectFlow activityEdge = this.createIn(ObjectFlow.class, structuredActivityNode1);

        InputPin inputPin = this.create(InputPin.class);
        nodeToDrop.getInputValues().add(inputPin);
        activityEdge.setTarget(inputPin);
        OutputPin outputPin = this.create(OutputPin.class);
        opaqueAction1.getOutputValues().add(outputPin);
        activityEdge.setSource(outputPin);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop,
                structuredActivityNode1, structuredActivityNode2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertEquals(root, activityEdge.getOwner());
    }

    /**
     * Test dropping an ActivityPartition from an ActivityPartition to an Activity:
     * ActivityPartition#subPartition feature should be empty and
     * Activity#ownedGroup and Activity#partition features should be set.
     */
    @Test
    public void testActivityPartitionDropFromActivityPartitionToActivity() {
        Activity root = this.create(Activity.class);
        ActivityPartition partition1 = this.createIn(ActivityPartition.class, root);
        ActivityPartition partitionToDrop = this.create(ActivityPartition.class);
        partition1.getSubpartitions().add(partitionToDrop);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(partitionToDrop,
                partition1, root, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(partition1.getSubpartitions().isEmpty());
        assertFalse(root.getOwnedGroups().isEmpty());
        assertFalse(root.getPartitions().isEmpty());
        assertEquals(root, partitionToDrop.getInActivity());
        assertEquals(root, partitionToDrop.getOwner());
        assertNull(partitionToDrop.getSuperPartition());
    }

    /**
     * Test dropping an ActivityPartition from an ActivityPartition to another
     * ActivityPartition: ActivityPartition#subPartition feature should be modified.
     */
    @Test
    public void testActivityPartitionDropFromActivityPartitionToActivityPartition() {
        Activity root = this.create(Activity.class);
        ActivityPartition partition1 = this.createIn(ActivityPartition.class, root);
        ActivityPartition partition2 = this.createIn(ActivityPartition.class, root);
        ActivityPartition partitionToDrop = this.create(ActivityPartition.class);
        partition1.getSubpartitions().add(partitionToDrop);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(partitionToDrop,
                partition1, partition2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(partition1.getSubpartitions().isEmpty());
        assertFalse(partition2.getSubpartitions().isEmpty());
        assertEquals(partition2, partitionToDrop.getOwner());
        assertEquals(List.of(partitionToDrop), partition2.getSubpartitions());
    }

    /**
     * Test dropping an ActivityPartition contained in an ActivityPartition to
     * another ActivityPartition. Initial container and new container are both
     * contained in different Activity. Activity#ownedGroup and Activity#partition
     * features should be modified. The owner of the elements inside the partition
     * should be modified.
     */
    @Test
    public void testActivityPartitionDropFromActivityPartitionToActivityPartitionInDifferentSubActivity() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        ActivityPartition partition1 = this.create(ActivityPartition.class);
        subActivity1.getOwnedGroups().add(partition1);
        subActivity1.getPartitions().add(partition1);
        Activity subActivity2 = this.createIn(Activity.class, root);
        ActivityPartition partition2 = this.create(ActivityPartition.class);
        subActivity2.getOwnedGroups().add(partition2);
        subActivity2.getPartitions().add(partition2);

        ActivityPartition partitionToDrop = this.create(ActivityPartition.class);
        partition1.getSubpartitions().add(partitionToDrop);

        OpaqueAction partitionContent1 = this.create(OpaqueAction.class);
        partitionContent1.getInPartitions().add(partitionToDrop);
        subActivity1.getOwnedNodes().add(partitionContent1);
        AcceptEventAction partitionContent2 = this.create(AcceptEventAction.class);
        partitionContent2.getInPartitions().add(partitionToDrop);
        subActivity1.getOwnedNodes().add(partitionContent2);

        ObjectFlow partitionContent3 = this.create(ObjectFlow.class);
        partitionContent3.getInPartitions().add(partitionToDrop);
        subActivity1.getEdges().add(partitionContent3);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(partitionToDrop,
                partition1, partition2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(partition1.getSubpartitions().isEmpty());
        assertFalse(partition2.getSubpartitions().isEmpty());
        assertEquals(partition2, partitionToDrop.getOwner());
        assertEquals(List.of(partitionToDrop), partition2.getSubpartitions());

        assertEquals(List.of(partitionToDrop), partitionContent1.getInPartitions());
        assertEquals(List.of(partitionToDrop), partitionContent2.getInPartitions());
        // Use contains instead of list comparison, order may not be preserved.
        assertTrue(subActivity2.getOwnedNodes().contains(partitionContent1));
        assertTrue(subActivity2.getOwnedNodes().contains(partitionContent2));
        assertTrue(subActivity2.getEdges().contains(partitionContent3));
        assertTrue(subActivity1.getOwnedNodes().isEmpty());
        assertTrue(subActivity1.getEdges().isEmpty());

    }

    /**
     * Test dropping an ActivityPartition contained in an ActivityPartition to
     * another ActivityPartition. Initial container and new container are both
     * contained in the same Activity. Activity#ownedGropu and Activity#partition
     * features should not be modified. The owner of the elements inside the
     * partition should not be modified.
     */
    @Test
    public void testActivityPartitionDropFromActivityPartitionToActivityPartitionInSameSubActivity() {
        Activity root = this.create(Activity.class);
        Activity subActivity = this.createIn(Activity.class, root);
        ActivityPartition partition1 = this.create(ActivityPartition.class);
        subActivity.getOwnedGroups().add(partition1);
        subActivity.getPartitions().add(partition1);
        ActivityPartition partition2 = this.create(ActivityPartition.class);
        subActivity.getOwnedGroups().add(partition2);
        subActivity.getPartitions().add(partition2);

        ActivityPartition partitionToDrop = this.create(ActivityPartition.class);
        partition1.getSubpartitions().add(partitionToDrop);

        OpaqueAction partitionContent1 = this.create(OpaqueAction.class);
        partitionContent1.getInPartitions().add(partitionToDrop);
        subActivity.getOwnedNodes().add(partitionContent1);
        AcceptEventAction partitionContent2 = this.create(AcceptEventAction.class);
        partitionContent2.getInPartitions().add(partitionToDrop);
        subActivity.getOwnedNodes().add(partitionContent2);

        ObjectFlow partitionContent3 = this.create(ObjectFlow.class);
        partitionContent3.getInPartitions().add(partitionToDrop);
        subActivity.getEdges().add(partitionContent3);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(partitionToDrop,
                partition1, partition2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(partition1.getSubpartitions().isEmpty());
        assertFalse(partition2.getSubpartitions().isEmpty());
        assertEquals(partition2, partitionToDrop.getOwner());
        assertEquals(List.of(partitionToDrop), partition2.getSubpartitions());

        assertEquals(List.of(partitionToDrop), partitionContent1.getInPartitions());
        assertEquals(List.of(partitionToDrop), partitionContent2.getInPartitions());
        // Check that no side effect happened on subActivity references.
        assertTrue(subActivity.getOwnedNodes().contains(partitionContent1));
        assertTrue(subActivity.getOwnedNodes().contains(partitionContent2));
        assertTrue(subActivity.getEdges().contains(partitionContent3));
    }

    /**
     * Test dropping an ActivityPartition from an Activity to an ActivityPartition:
     * Activity#ownedGroup and Activity#partition features should be empty and
     * ActivityPartition#subPartition should be set.
     */
    @Test
    public void testActivityPartitionDropFromActivityToActivityPartition() {
        Activity root = this.create(Activity.class);
        ActivityPartition partition1 = this.createIn(ActivityPartition.class, root);
        ActivityPartition partitionToDrop = this.create(ActivityPartition.class);
        root.getOwnedGroups().add(partitionToDrop);
        root.getPartitions().add(partitionToDrop);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(partitionToDrop, root,
                partition1, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertFalse(root.getOwnedGroups().contains(partitionToDrop));
        assertFalse(root.getPartitions().contains(partitionToDrop));
        assertFalse(partition1.getSubpartitions().isEmpty());
        assertEquals(List.of(partitionToDrop), partition1.getSubpartitions());
        assertEquals(partition1, partitionToDrop.getOwner());
    }

    /**
     * Test dropping an ActivityPartition from an Activity to a non-Activity,
     * non-ActivityPartition. The DnD should fail.
     */
    @Test
    public void testActivityPartitionDropFromActivityToComment() {
        Activity root = this.create(Activity.class);
        ActivityPartition partitionToDrop = this.create(ActivityPartition.class);
        root.getOwnedGroups().add(partitionToDrop);
        root.getPartitions().add(partitionToDrop);

        Comment newContainer = this.create(Comment.class);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(partitionToDrop, root,
                newContainer, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
    }

    /**
     * Test dropping an ActivityPartition from an Activity to the same Activity: It
     * should failed and nothing happens.
     */
    @Test
    public void testActivityPartitionDropFromActivityToSameActivity() {
        Activity root = this.create(Activity.class);
        ActivityPartition partitionToDrop = this.create(ActivityPartition.class);
        root.getOwnedGroups().add(partitionToDrop);
        root.getPartitions().add(partitionToDrop);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(partitionToDrop, root,
                root, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
    }

    /**
     * Test dropping an ActivityPartition from an Activity to another Activity:
     * Activity#ownedGroup and Activity#partition features should be modified. The
     * owner of the elements inside the partition should be modified.
     */
    @Test
    public void testActivityPartitionDropFromSubActivityToSubActivity() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        Activity subActivity2 = this.createIn(Activity.class, root);
        ActivityPartition partitionToDrop = this.create(ActivityPartition.class);
        subActivity1.getOwnedGroups().add(partitionToDrop);
        subActivity1.getPartitions().add(partitionToDrop);

        OpaqueAction partitionContent1 = this.create(OpaqueAction.class);
        partitionContent1.getInPartitions().add(partitionToDrop);
        subActivity1.getOwnedNodes().add(partitionContent1);
        AcceptEventAction partitionContent2 = this.create(AcceptEventAction.class);
        partitionContent2.getInPartitions().add(partitionToDrop);
        subActivity1.getOwnedNodes().add(partitionContent2);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(partitionToDrop,
                subActivity1, subActivity2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(subActivity1.getOwnedGroups().isEmpty());
        assertTrue(subActivity1.getPartitions().isEmpty());
        assertFalse(subActivity2.getOwnedGroups().isEmpty());
        assertFalse(subActivity2.getPartitions().isEmpty());
        assertEquals(subActivity2, partitionToDrop.getInActivity());
        assertEquals(subActivity2, partitionToDrop.getOwner());

        assertEquals(List.of(partitionToDrop), partitionContent1.getInPartitions());
        assertEquals(List.of(partitionToDrop), partitionContent2.getInPartitions());
        assertEquals(List.of(partitionContent1, partitionContent2), subActivity2.getOwnedNodes());
        assertTrue(subActivity1.getOwnedNodes().isEmpty());
    }

    /**
     * Test dropping an ActivityPartition contained in an ActivityPartition to
     * another ActivityPartition. Initial container and new container are both
     * contained in different Activity. Dropped ActivityPartition contains another
     * ActivityPartition. Activity#ownedGroup and Activity#partition features should
     * be modified. The owner of the elements inside the partition should be
     * modified.
     */
    @Test
    public void testActivityPartitionWithSubPartitionDropFromActivityPartitionToActivityPartitionInDifferentSubActivity() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        ActivityPartition partition1 = this.create(ActivityPartition.class);
        subActivity1.getOwnedGroups().add(partition1);
        subActivity1.getPartitions().add(partition1);
        Activity subActivity2 = this.createIn(Activity.class, root);
        ActivityPartition partition2 = this.create(ActivityPartition.class);
        subActivity2.getOwnedGroups().add(partition2);
        subActivity2.getPartitions().add(partition2);

        ActivityPartition partitionToDrop = this.create(ActivityPartition.class);
        partition1.getSubpartitions().add(partitionToDrop);

        OpaqueAction partitionContent1 = this.create(OpaqueAction.class);
        partitionContent1.getInPartitions().add(partitionToDrop);
        subActivity1.getOwnedNodes().add(partitionContent1);

        ActivityPartition subPartitionToDrop = this.create(ActivityPartition.class);
        partitionToDrop.getSubpartitions().add(subPartitionToDrop);

        AcceptEventAction partitionContent2 = this.create(AcceptEventAction.class);
        partitionContent2.getInPartitions().add(subPartitionToDrop);
        subActivity1.getOwnedNodes().add(partitionContent2);

        ObjectFlow partitionContent3 = this.create(ObjectFlow.class);
        partitionContent3.getInPartitions().add(subPartitionToDrop);
        subActivity1.getEdges().add(partitionContent3);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(partitionToDrop,
                partition1, partition2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(partition1.getSubpartitions().isEmpty());
        assertFalse(partition2.getSubpartitions().isEmpty());
        assertEquals(partition2, partitionToDrop.getOwner());
        assertEquals(List.of(partitionToDrop), partition2.getSubpartitions());

        assertEquals(List.of(partitionToDrop), partitionContent1.getInPartitions());
        assertEquals(List.of(subPartitionToDrop), partitionContent2.getInPartitions());
        // Use contains instead of list comparison, order may not be preserved.
        assertTrue(subActivity2.getOwnedNodes().contains(partitionContent1));
        assertTrue(subActivity2.getOwnedNodes().contains(partitionContent2));
        assertTrue(subActivity2.getEdges().contains(partitionContent3));
        assertTrue(subActivity1.getOwnedNodes().isEmpty());
        assertTrue(subActivity1.getEdges().isEmpty());
    }

    /**
     * Test dropping an {@link StructuredActivityNode} from an Activity to another
     * Activity: The Activity container should changed.
     */
    @Test
    public void testStructuredActivityNodeDropFromActivityToActivity() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        Activity subActivity2 = this.createIn(Activity.class, root);
        StructuredActivityNode nodeToDrop = this.create(StructuredActivityNode.class);
        nodeToDrop.setActivity(subActivity1);

        Status status = new ActivityInternalSourceToRepresentationDropBehaviorProvider().drop(nodeToDrop, subActivity1,
                subActivity2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertFalse(subActivity1.getStructuredNodes().contains(nodeToDrop));
        assertTrue(subActivity2.getStructuredNodes().contains(nodeToDrop));
    }
}
