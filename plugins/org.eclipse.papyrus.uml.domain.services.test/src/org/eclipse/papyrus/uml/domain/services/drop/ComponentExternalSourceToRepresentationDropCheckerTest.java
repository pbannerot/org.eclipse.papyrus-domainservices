/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.ComponentExternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Clause;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Reception;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Test class for {@link ComponentExternalSourceToRepresentationDropChecker}.
 *
 * @author <a href="mailto:gwendal.daniel@obeosoft.com">Gwendal Daniel</a>
 */
public class ComponentExternalSourceToRepresentationDropCheckerTest extends AbstractUMLTest {

    private ComponentExternalSourceToRepresentationDropChecker componentExternalSourceToRepresentationDropChecker;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        this.componentExternalSourceToRepresentationDropChecker = new ComponentExternalSourceToRepresentationDropChecker();
    }

    @ParameterizedTest
    @MethodSource("getCanDragAndDropParameters")
    public void testCanDragAndDrop(Class<? extends Element> elementToDropClazz,
            Class<? extends Element> semanticContainerClazz, boolean expectedCheckStatus) {
        Element elementToDrop = this.create(elementToDropClazz);
        Element semanticContainer = this.create(semanticContainerClazz);

        CheckStatus canDragAndDropStatus = this.componentExternalSourceToRepresentationDropChecker
                .canDragAndDrop(elementToDrop, semanticContainer);
        assertEquals(expectedCheckStatus, canDragAndDropStatus.isValid());
    }

    /**
     * Tests that a {@link Property} can be dropped on its containing
     * {@link Component}.
     */
    @Test
    public void testCanDragAndDropPropertyOnContainingComponent() {
        Property property = this.create(Property.class);
        Component component = this.create(Component.class);
        component.getOwnedAttributes().add(property);

        CheckStatus canDragAndDropStatus = this.componentExternalSourceToRepresentationDropChecker
                .canDragAndDrop(property, component);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Tests that a {@link Property} can be dropped on its containing
     * {@link Interface}.
     */
    @Test
    public void testCanDragAndDropPropertyOnContainingInterface() {
        Property property = this.create(Property.class);
        Interface interfaceElement = this.create(Interface.class);
        interfaceElement.getOwnedAttributes().add(property);

        CheckStatus canDragAndDropStatus = this.componentExternalSourceToRepresentationDropChecker
                .canDragAndDrop(property, interfaceElement);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Tests that a {@link Property} can be dropped on a {@link Property} typed with
     * its container.
     */
    @Test
    public void testCanDragAndDropPropertyOnContainingTypedProperty() {
        Property property = this.create(Property.class);
        Property typedProperty = this.create(Property.class);
        Component component = this.create(Component.class);
        typedProperty.setType(component);
        component.getOwnedAttributes().add(property);

        CheckStatus canDragAndDropStatus = this.componentExternalSourceToRepresentationDropChecker
                .canDragAndDrop(property, typedProperty);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Tests that a {@link Property} cannot be dropped on a {@link Property} typed
     * with a classifier that doesn't contain the property.
     */
    @Test
    public void testCanDragAndDropPropertyOnNonContainingTypedProperty() {
        Property property = this.create(Property.class);
        Property typedProperty = this.create(Property.class);
        Component component = this.create(Component.class);
        typedProperty.setType(component);

        CheckStatus canDragAndDropStatus = this.componentExternalSourceToRepresentationDropChecker
                .canDragAndDrop(property, typedProperty);
        assertFalse(canDragAndDropStatus.isValid());
    }

    public static Stream<Arguments> getCanDragAndDropParameters() {
        // @formatter:off
        return Stream.of(
                // Check that any classifier can be dropped on a Property/Port to type it.
                Arguments.of(Artifact.class, Port.class, true),
                Arguments.of(Artifact.class, Property.class, true),
                Arguments.of(Comment.class, Comment.class, false),
                Arguments.of(Comment.class, Package.class, true),
                Arguments.of(Component.class, Comment.class, false),
                Arguments.of(Component.class, Component.class, true),
                Arguments.of(Component.class, Package.class, true),
                Arguments.of(Component.class, Port.class, true),
                Arguments.of(Component.class, Property.class, true),
                Arguments.of(Constraint.class, Comment.class, false),
                Arguments.of(Constraint.class, Package.class, true),
                Arguments.of(Interface.class, Comment.class, false),
                Arguments.of(Interface.class, Package.class, true),
                Arguments.of(Interface.class, Port.class, true),
                Arguments.of(Interface.class, Property.class, true),
                Arguments.of(Operation.class, Comment.class, false),
                Arguments.of(Operation.class, Interface.class, true),
                Arguments.of(Package.class, Comment.class, false),
                Arguments.of(Package.class, Package.class, true),
                Arguments.of(Port.class, Comment.class, false),
                Arguments.of(Port.class, Component.class, true),
                Arguments.of(Port.class, Property.class, true),
                Arguments.of(Property.class, Comment.class, false),
                // False because the component doesn't contain the property
                Arguments.of(Property.class, Component.class, false),
                // False because the interface doesn't contain the property
                Arguments.of(Property.class, Interface.class, false),
                // False because a property cannot be dropped on a non-typed property
                Arguments.of(Property.class, Property.class, false),
                Arguments.of(Reception.class, Comment.class, false),
                Arguments.of(Reception.class, Interface.class, true),
                // False because a type can only be dropped on a property if it has no representation on the diagram
                Arguments.of(Activity.class, Component.class, false),
                // Test default case
                Arguments.of(Clause.class , Component.class, false)
                );
        // @formatter:on
    }

}
