/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.ComponentInternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Reception;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for
 * {@link ComponentInternalSourceToRepresentationDropBehaviorProvider}.
 *
 * @author <a href="mailto:gwendal.daniel@obeosoft.com">Gwendal Daniel</a>
 */
public class ComponentInternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    private ComponentInternalSourceToRepresentationDropBehaviorProvider componentInternalSourceToRepresentationDropBehaviorProvider;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        this.componentInternalSourceToRepresentationDropBehaviorProvider = new ComponentInternalSourceToRepresentationDropBehaviorProvider();
    }

    @Test
    public void testCommentDropFromPackageToPackage() {
        Comment commentToDrop = this.create(Comment.class);
        Package pack1 = this.create(Package.class);
        Package pack2 = this.create(Package.class);
        pack1.getOwnedComments().add(commentToDrop);

        Status status = this.componentInternalSourceToRepresentationDropBehaviorProvider.drop(commentToDrop, pack1,
                pack2, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertTrue(pack1.getOwnedComments().isEmpty());
        assertTrue(pack2.getOwnedComments().contains(commentToDrop));
    }

    @Test
    public void testComponentDropFromComponentToComponent() {
        Component componentToDrop = this.create(Component.class);
        Component component1 = this.create(Component.class);
        Component component2 = this.create(Component.class);
        component1.getPackagedElements().add(componentToDrop);

        Status status = this.componentInternalSourceToRepresentationDropBehaviorProvider.drop(componentToDrop,
                component1, component2, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertTrue(component1.getPackagedElements().isEmpty());
        assertTrue(component2.getPackagedElements().contains(componentToDrop));
    }

    @Test
    public void testComponentDropFromPackageToPackage() {
        Component componentToDrop = this.create(Component.class);
        Package pack1 = this.create(Package.class);
        Package pack2 = this.create(Package.class);
        pack1.getPackagedElements().add(componentToDrop);

        Status status = this.componentInternalSourceToRepresentationDropBehaviorProvider.drop(componentToDrop, pack1,
                pack2, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertTrue(pack1.getPackagedElements().isEmpty());
        assertTrue(pack2.getPackagedElements().contains(componentToDrop));
    }

    @Test
    public void testConstraintDropFromPackageToPackage() {
        Constraint constraintToDrop = this.create(Constraint.class);
        Package pack1 = this.create(Package.class);
        Package pack2 = this.create(Package.class);
        pack1.getOwnedRules().add(constraintToDrop);

        Status status = this.componentInternalSourceToRepresentationDropBehaviorProvider.drop(constraintToDrop, pack1,
                pack2, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertTrue(pack1.getOwnedRules().isEmpty());
        assertTrue(pack2.getOwnedRules().contains(constraintToDrop));
    }

    @Test
    public void testInterfaceDropFromPackageToPackage() {
        Interface interfaceToDrop = this.create(Interface.class);
        Package pack1 = this.create(Package.class);
        Package pack2 = this.create(Package.class);
        pack1.getPackagedElements().add(interfaceToDrop);

        Status status = this.componentInternalSourceToRepresentationDropBehaviorProvider.drop(interfaceToDrop, pack1,
                pack2, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertTrue(pack1.getPackagedElements().isEmpty());
        assertTrue(pack2.getPackagedElements().contains(interfaceToDrop));
    }

    @Test
    public void testOperationDropFromInterfaceToInterface() {
        Operation operationToDrop = this.create(Operation.class);
        Interface interface1 = this.create(Interface.class);
        Interface interface2 = this.create(Interface.class);
        interface1.getOwnedOperations().add(operationToDrop);

        Status status = this.componentInternalSourceToRepresentationDropBehaviorProvider.drop(operationToDrop,
                interface1, interface2, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertTrue(interface1.getOwnedOperations().isEmpty());
        assertTrue(interface2.getOwnedOperations().contains(operationToDrop));
    }

    @Test
    public void testPackageDropFromPackageToPackage() {
        Package packageToDrop = this.create(Package.class);
        Package pack1 = this.create(Package.class);
        Package pack2 = this.create(Package.class);
        pack1.getPackagedElements().add(packageToDrop);

        Status status = this.componentInternalSourceToRepresentationDropBehaviorProvider.drop(packageToDrop, pack1,
                pack2, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertTrue(pack1.getPackagedElements().isEmpty());
        assertTrue(pack2.getPackagedElements().contains(packageToDrop));
    }

    @Test
    public void testPropertyDropFromComponentToComponent() {
        Property propertyToDrop = this.create(Property.class);
        Component component1 = this.create(Component.class);
        Component component2 = this.create(Component.class);
        component1.getOwnedAttributes().add(propertyToDrop);

        Status status = this.componentInternalSourceToRepresentationDropBehaviorProvider.drop(propertyToDrop,
                component1, component2, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertTrue(component1.getOwnedAttributes().isEmpty());
        assertTrue(component2.getOwnedAttributes().contains(propertyToDrop));
    }

    @Test
    public void testPropertyDropFromComponentToInterface() {
        Property propertyToDrop = this.create(Property.class);
        Component component = this.create(Component.class);
        Interface interfaceElement = this.create(Interface.class);
        component.getOwnedAttributes().add(propertyToDrop);

        Status status = this.componentInternalSourceToRepresentationDropBehaviorProvider.drop(propertyToDrop, component,
                interfaceElement, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertTrue(component.getOwnedAttributes().contains(propertyToDrop));
        assertTrue(interfaceElement.getOwnedAttributes().isEmpty());
    }

    @Test
    public void testPropertyDropFromComponentToTypedProperty() {
        Property propertyToDrop = this.create(Property.class);
        Component component = this.create(Component.class);
        component.getOwnedAttributes().add(propertyToDrop);

        Property typedProperty = this.create(Property.class);
        Component component2 = this.create(Component.class);
        typedProperty.setType(component2);

        Status status = this.componentInternalSourceToRepresentationDropBehaviorProvider.drop(propertyToDrop, component,
                typedProperty, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertFalse(component.getOwnedAttributes().contains(propertyToDrop));
        assertTrue(component2.getOwnedAttributes().contains(propertyToDrop));
    }

    @Test
    public void testPropertyDropFromInterfaceToComponent() {
        Property propertyToDrop = this.create(Property.class);
        Interface interfaceElement = this.create(Interface.class);
        Component component = this.create(Component.class);
        interfaceElement.getOwnedAttributes().add(propertyToDrop);

        Status status = this.componentInternalSourceToRepresentationDropBehaviorProvider.drop(propertyToDrop,
                interfaceElement, component, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        // Check that no side effect happened.
        assertTrue(interfaceElement.getOwnedAttributes().contains(propertyToDrop));
        assertTrue(component.getOwnedAttributes().isEmpty());
    }

    @Test
    public void testPropertyDropFromInterfaceToInterface() {
        Property propertyToDrop = this.create(Property.class);
        Interface interface1 = this.create(Interface.class);
        Interface interface2 = this.create(Interface.class);
        interface1.getOwnedAttributes().add(propertyToDrop);

        Status status = this.componentInternalSourceToRepresentationDropBehaviorProvider.drop(propertyToDrop,
                interface1, interface2, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertTrue(interface1.getOwnedAttributes().isEmpty());
        assertTrue(interface2.getOwnedAttributes().contains(propertyToDrop));
    }

    @Test
    public void testReceptionDropFromInterfaceToInterface() {
        Reception receptionToDrop = this.create(Reception.class);
        Interface interface1 = this.create(Interface.class);
        Interface interface2 = this.create(Interface.class);
        interface1.getOwnedReceptions().add(receptionToDrop);

        Status status = this.componentInternalSourceToRepresentationDropBehaviorProvider.drop(receptionToDrop,
                interface1, interface2, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertTrue(interface1.getOwnedReceptions().isEmpty());
        assertTrue(interface2.getOwnedReceptions().contains(receptionToDrop));
    }

}
