/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collections;
import java.util.Set;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.PackageExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Abstraction;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for
 * {@link PackageExternalSourceToRepresentationDropBehaviorProvider}.
 *
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class PackageExternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    private PackageExternalSourceToRepresentationDropBehaviorProvider packageExternalSourceToRepresentationDropBehaviorProvider;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        this.packageExternalSourceToRepresentationDropBehaviorProvider = new PackageExternalSourceToRepresentationDropBehaviorProvider();
    }

    /**
     * Test dropping a {@link Abstraction} on a {@link Package}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testAbstractionDropOnPackage() {
        Abstraction abstraction = this.create(Abstraction.class);
        Package pack = this.create(Package.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(abstraction, pack,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(abstraction), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Comment} on a {@link Comment}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testCommentDropOnComment() {
        Comment comment1 = this.create(Comment.class);
        Comment comment2 = this.create(Comment.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(comment1, comment2,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Comment} on a {@link Constraint}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testCommentDropOnConstraint() {
        Comment comment = this.create(Comment.class);
        Constraint constraint = this.create(Constraint.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(comment, constraint,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Comment} on a {@link Model}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testCommentDropOnModel() {
        Comment comment = this.create(Comment.class);
        Model model = this.create(Model.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(comment, model,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(comment), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Comment} on a {@link Package}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testCommentDropOnPackage() {
        Comment comment = this.create(Comment.class);
        Package pack = this.create(Package.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(comment, pack,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(comment), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Constraint} on a {@link Comment}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testConstraintDropOnComment() {
        Constraint constraint = this.create(Constraint.class);
        Comment comment = this.create(Comment.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(constraint, comment,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Constraint} on a {@link Constraint}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testConstraintDropOnConstraint() {
        Constraint constraint1 = this.create(Constraint.class);
        Constraint constraint2 = this.create(Constraint.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(constraint1, constraint2,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Constraint} on a {@link Model}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testConstraintDropOnModel() {
        Constraint constraint = this.create(Constraint.class);
        Model model = this.create(Model.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(constraint, model,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(constraint), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Constraint} on a {@link Package}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testConstraintDropOnPackage() {
        Constraint constraint = this.create(Constraint.class);
        Package pack = this.create(Package.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(constraint, pack,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(constraint), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Dependency} on a {@link Package}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testDependencyDropOnPackage() {
        Dependency dependency = this.create(Dependency.class);
        Package pack = this.create(Package.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(dependency, pack,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(dependency), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Model} on a {@link Comment}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testModelDropOnComment() {
        Model model = this.create(Model.class);
        Comment comment = this.create(Comment.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(model, comment,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Model} on a {@link Constraint}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testModelDropOnConstraint() {
        Model model = this.create(Model.class);
        Constraint constraint = this.create(Constraint.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(model, constraint,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Model} on a {@link Model}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testModelDropOnModel() {
        Model model1 = this.create(Model.class);
        Model model2 = this.create(Model.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(model1, model2,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(model1), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Model} on a {@link Package}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testModelDropOnPackage() {
        Model model = this.create(Model.class);
        Package pack = this.create(Package.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(model, pack,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(model), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Package} on a {@link Comment}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testPackageDropOnComment() {
        Package pack = this.create(Package.class);
        Comment comment = this.create(Comment.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(pack, comment,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Package} on a {@link Constraint}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testPackageDropOnConstraint() {
        Package pack = this.create(Package.class);
        Constraint constraint = this.create(Constraint.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(pack, constraint,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Package} on a {@link Model}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testPackageDropOnModel() {
        Package pack = this.create(Package.class);
        Model model = this.create(Model.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(pack, model,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(pack), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Package} on a {@link Package}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testPackageDropOnPackage() {
        Package pack1 = this.create(Package.class);
        Package pack2 = this.create(Package.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(pack1, pack2,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(pack1), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link PackageImport} on a {@link Package}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testPackageImportDropOnPackage() {
        PackageImport packageImport = this.create(PackageImport.class);
        Package pack = this.create(Package.class);

        DnDStatus status = this.packageExternalSourceToRepresentationDropBehaviorProvider.drop(packageImport, pack,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(packageImport), status.getElementsToDisplay());
    }
}
