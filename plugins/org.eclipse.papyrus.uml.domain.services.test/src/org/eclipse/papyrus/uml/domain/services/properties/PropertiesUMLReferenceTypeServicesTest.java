/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.properties;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.papyrus.uml.domain.services.properties.mock.MockLogger;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.DurationConstraint;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.resource.UML302UMLResource;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link PropertiesUMLReferenceTypeServices} service class.
 *
 * @author <a href="mailto:jerome.gout@obeosoft.fr">Jerome Gout</a>
 */
public class PropertiesUMLReferenceTypeServicesTest extends AbstractPropertiesServicesTest {

    /**
     * URI of the UML model.
     */
    private static final String UML_MODEL_URI = "platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore";

    private static EPackage uml2EPackage;

    private PropertiesUMLReferenceTypeServices services;

    @BeforeAll
    public static void loadUMLModel() {
        URI uri = URI.createURI(UML_MODEL_URI, true);
        Resource resource = UML302UMLResource.Factory.INSTANCE.createResource(uri);
        try {
            resource.load(null);
        } catch (IOException exception) {
            throw new WrappedException(exception);
        }
        uml2EPackage = (EPackage) resource.getContents().get(0);
    }

    @BeforeEach
    public void init() {
        services = new PropertiesUMLReferenceTypeServices(new MockLogger());
        services.initRedefinedTypes(uml2EPackage);
    }

    /**
     * UML Model defines a redefinition of the type of the {@code specification}
     * reference of {@code DurationConstraint} element.<br>
     * The definition of this reference found in the {@code Constraint} element
     * specifies that its type should be a {@code ValueSpecification}. Nevertheless,
     * inside the {@code DurationConstraint} element this type is redefined to be a
     * {@code DurationInterval} instead. <br>
     * The purpose of this test is to validate that the type of the
     * {@code specification} reference of {@code DurationConstraint} element is
     * actually {@code DurationInterval}.
     * 
     */
    @Test
    public void testRedefinedType() {
        DurationConstraint dc = create(DurationConstraint.class);
        String redefinedType = services.getFeatureTypeQualifiedName(dc, UMLPackage.eINSTANCE.getConstraint_Specification().getName());
        assertEquals("uml::DurationInterval", redefinedType);
    }

    /**
     * The purpose of this test is to validate that when there is not redefinition
     * of the type of a reference, the type returned is one specified in the
     * reference.
     */
    @Test
    public void testNotRedefinedType() {
        Activity activity = create(Activity.class);
        String redefinedType = services.getFeatureTypeQualifiedName(activity, UMLPackage.eINSTANCE.getActivity_Variable().getName());
        assertEquals("uml::Variable", redefinedType);
    }
}
