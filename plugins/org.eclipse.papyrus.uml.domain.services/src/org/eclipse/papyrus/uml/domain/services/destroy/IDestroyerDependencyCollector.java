/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.destroy;

import java.util.Set;

import org.eclipse.emf.ecore.EObject;

/**
 * Object in charge of collecting the dependency of an element that needs to be
 * deleted with the given element.
 * 
 * @author Arthur Daussy
 *
 */
public interface IDestroyerDependencyCollector {

    /**
     * Collects the dependencies that needs to be deleted with the given elements.
     * 
     * @param source
     *               a source object
     * @return a {@link Set} of dependency (the current element does not belong to
     *         that list)
     */
    Set<EObject> collectDependencies(EObject source);

}