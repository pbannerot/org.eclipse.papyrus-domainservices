/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.directedit;

import org.eclipse.emf.ecore.EObject;

/**
 * Service in charge of providing a input label for the direct edit tool.
 * 
 * @author Arthur daussy
 *
 */
public interface IDirectEditInputValueProvider {

    /**
     * Gets the text to display at the beginning of a direct edit tool execution.
     * 
     * @param eObject
     *                the semantic object
     * @return a label (never <code>null</code>)
     */
    String getDirectEditInputValue(EObject eObject);

}
