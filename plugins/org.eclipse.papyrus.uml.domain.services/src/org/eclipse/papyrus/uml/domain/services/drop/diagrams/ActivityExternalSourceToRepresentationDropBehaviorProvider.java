/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.util.Collections;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.drop.DnDStatus;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ActivityGroup;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.ActivityParameterNode;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.ExpansionNode;
import org.eclipse.uml2.uml.ExpansionRegion;
import org.eclipse.uml2.uml.InterruptibleActivityRegion;
import org.eclipse.uml2.uml.StructuredActivityNode;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Drop behavior provider of a semantic drop (from Model Explorer view) to an
 * Activity Diagram Element (or the root of the diagram itself).
 * 
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class ActivityExternalSourceToRepresentationDropBehaviorProvider
        implements IExternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public DnDStatus drop(EObject droppedElement, EObject target, ECrossReferenceAdapter crossRef,
            IEditableChecker editableChecker) {
        return new ActivityDropInsideRepresentationBehaviorProviderSwitch(target).doSwitch(droppedElement);
    }

    static class ActivityDropInsideRepresentationBehaviorProviderSwitch extends UMLSwitch<DnDStatus> {

        private final EObject target;

        ActivityDropInsideRepresentationBehaviorProviderSwitch(EObject target) {
            super();
            this.target = target;
        }

        @Override
        public DnDStatus caseActivity(Activity activity) {
            if (this.target instanceof Activity) {
                return DnDStatus.createNothingStatus(Set.of(activity));
            }
            return super.caseActivity(activity);
        }

        @Override
        public DnDStatus caseActivityEdge(ActivityEdge activityEdge) {
                return DnDStatus.createNothingStatus(Set.of(activityEdge));
        }

        @Override
        public DnDStatus caseActivityNode(ActivityNode activityNode) {
            if (this.target instanceof Activity || this.target instanceof ActivityGroup) {
                return DnDStatus.createNothingStatus(Set.of(activityNode));
            }
            return super.caseActivityNode(activityNode);
        }

        @Override
        public DnDStatus caseActivityParameterNode(ActivityParameterNode activityParameterNode) {
            if (this.target instanceof Activity) {
                return DnDStatus.createNothingStatus(Set.of(activityParameterNode));
            }
            return DnDStatus.createFailingStatus("ActivityParameterNode can only be drag and drop on an Activity.",
                    Collections.emptySet());
        }

        @Override
        public DnDStatus caseActivityPartition(ActivityPartition activityPartition) {
            if (this.target instanceof Activity || this.target instanceof ActivityPartition) {
                return DnDStatus.createNothingStatus(Set.of(activityPartition));
            }
            return super.caseActivityPartition(activityPartition);
        }

        @Override
        public DnDStatus caseComment(Comment comment) {
            if (this.target instanceof Activity || this.target instanceof ActivityGroup) {
                return DnDStatus.createNothingStatus(Set.of(comment));
            }
            return super.caseComment(comment);
        }

        @Override
        public DnDStatus caseConstraint(Constraint constraint) {
            if (this.target instanceof Activity || this.target instanceof StructuredActivityNode) {
                return DnDStatus.createNothingStatus(Set.of(constraint));
            }
            return super.caseConstraint(constraint);
        }

        @Override
        public DnDStatus caseExpansionNode(ExpansionNode expansionNode) {
            if (this.target instanceof ExpansionRegion) {
                return DnDStatus.createNothingStatus(Set.of(expansionNode));
            }
            return DnDStatus.createFailingStatus("ExpansionNode can only be drag and drop on an ExpansionRegion.",
                    Collections.emptySet());
        }

        @Override
        public DnDStatus caseInterruptibleActivityRegion(InterruptibleActivityRegion interruptibleActivityRegion) {
            if (this.target instanceof Activity) {
                return DnDStatus.createNothingStatus(Set.of(interruptibleActivityRegion));
            }
            return super.caseInterruptibleActivityRegion(interruptibleActivityRegion);
        }

        @Override
        public DnDStatus defaultCase(EObject object) {
            return DnDStatus.createFailingStatus("DnD is forbidden.", Collections.emptySet());
        }

    }

}
