/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.util.Collections;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.create.CreationStatus;
import org.eclipse.papyrus.uml.domain.services.create.ElementConfigurer;
import org.eclipse.papyrus.uml.domain.services.create.ElementCreator;
import org.eclipse.papyrus.uml.domain.services.drop.DnDStatus;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.modify.ElementFeatureModifier;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DurationObservation;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.TimeObservation;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Drop behavior provider of a semantic drop (from Model Explorer view) to a
 * Communication Diagram Element (or the root of the diagram itself).
 * 
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 *
 */
public class CommunicationExternalSourceToRepresentationDropBehaviorProvider
        implements IExternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public DnDStatus drop(EObject droppedElement, EObject target, ECrossReferenceAdapter crossRef,
            IEditableChecker editableChecker) {
        return new CommunicationDropInsideRepresentationBehaviorProviderSwitch(target, crossRef, editableChecker)
                .doSwitch(droppedElement);
    }

    static class CommunicationDropInsideRepresentationBehaviorProviderSwitch extends UMLSwitch<DnDStatus> {

        private final EObject target;

        private final ECrossReferenceAdapter crossRef;

        private final IEditableChecker editableChecker;

        CommunicationDropInsideRepresentationBehaviorProviderSwitch(EObject target, ECrossReferenceAdapter crossRef,
                IEditableChecker editableChecker) {
            super();
            this.target = target;
            this.crossRef = crossRef;
            this.editableChecker = editableChecker;
        }

        @Override
        public DnDStatus caseComment(Comment comment) {
            if (this.target instanceof Interaction) {
                return DnDStatus.createNothingStatus(Set.of(comment));
            }
            return super.caseComment(comment);
        }

        @Override
        public DnDStatus caseConstraint(Constraint constraint) {
            if (this.target instanceof Interaction) {
                return DnDStatus.createNothingStatus(Set.of(constraint));
            }
            return super.caseConstraint(constraint);
        }

        @Override
        public DnDStatus caseDurationObservation(DurationObservation durationObservation) {
            if (this.target instanceof Interaction) {
                return DnDStatus.createNothingStatus(Set.of(durationObservation));
            }
            return super.caseDurationObservation(durationObservation);
        }

        @Override
        public DnDStatus caseLifeline(Lifeline lifeline) {
            if (this.target instanceof Interaction) {
                return DnDStatus.createNothingStatus(Set.of(lifeline));
            }
            return super.caseLifeline(lifeline);
        }

        @Override
        public DnDStatus caseMessage(Message message) {
            // Since Message is an Edge, it is always DnD, whatever the target mapping
            return DnDStatus.createNothingStatus(Set.of(message));
        }

        @Override
        public DnDStatus caseProperty(Property property) {
            // Drop a Property on a Lifeline will type (represents) the lifeline
            if (this.target instanceof Lifeline) {
                ((Lifeline) this.target).setRepresents(property);
                return DnDStatus.createOKStatus(Collections.emptySet());
            }
            return super.caseProperty(property);
        }

        @Override
        public DnDStatus caseTimeObservation(TimeObservation timeObservation) {
            if (this.target instanceof Interaction) {
                return DnDStatus.createNothingStatus(Set.of(timeObservation));
            }
            return super.caseTimeObservation(timeObservation);
        }

        @Override
        public DnDStatus caseType(Type type) {
            if (this.target instanceof Interaction || this.target instanceof Lifeline) {
                ElementFeatureModifier featureModifier = new ElementFeatureModifier(this.crossRef,
                        this.editableChecker);
                ElementCreator elementCreator = new ElementCreator(new ElementConfigurer(), featureModifier);
                Lifeline lifeLineToDrop = null;
                Property propertyToRepresents = null;
                if (this.target instanceof Interaction) {
                    // Drop a Classifier on an Interaction : create a new lifeline, a new Property
                    // in the interaction typed by the dropped classifier and the new Lifeline will
                    // represent this property.
                    CreationStatus createProperty = elementCreator.create(this.target,
                            UMLPackage.eINSTANCE.getProperty().getName(),
                            UMLPackage.eINSTANCE.getStructuredClassifier_OwnedAttribute().getName());
                    propertyToRepresents = (Property) createProperty.getElement();
                    CreationStatus createLifeline = elementCreator.create(this.target,
                            UMLPackage.eINSTANCE.getLifeline().getName(),
                            UMLPackage.eINSTANCE.getInteraction_Lifeline().getName());
                    lifeLineToDrop = (Lifeline) createLifeline.getElement();
                } else {
                    // Drop a Classifier on a Lifeline : create a new Property in the interaction
                    // typed by the dropped classifier and the target Lifeline will represent this
                    // property.
                    CreationStatus createProperty = elementCreator.create(this.getInteraction(this.target),
                            UMLPackage.eINSTANCE.getProperty().getName(),
                            UMLPackage.eINSTANCE.getStructuredClassifier_OwnedAttribute().getName());
                    propertyToRepresents = (Property) createProperty.getElement();
                    lifeLineToDrop = (Lifeline) this.target;
                }
                propertyToRepresents.setType(type);
                lifeLineToDrop.setRepresents(propertyToRepresents);
                DnDStatus dnDStatus = null;
                if (this.target instanceof Interaction) {
                    dnDStatus = DnDStatus.createOKStatus(Set.of(lifeLineToDrop));
                } else {
                    dnDStatus = DnDStatus.createOKStatus(Collections.emptySet());
                }
                return dnDStatus;
            }
            return super.caseType(type);
        }

        @Override
        public DnDStatus defaultCase(EObject obj) {
            return DnDStatus.createFailingStatus("DnD is forbidden.", Collections.emptySet());
        }

        private EObject getInteraction(EObject source) {
            for (EObject element = source; element != null; element = element.eContainer()) {
                if (element instanceof Interaction) {
                    return element;
                }
            }
            return null;
        }

    }

}
