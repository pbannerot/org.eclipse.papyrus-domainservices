/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DurationObservation;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.TimeObservation;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Check semantic drop (from Explorer view) to a Communication Diagram Element
 * (or the root of the diagram itself).
 * 
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 *
 */
public class CommunicationExternalSourceToRepresentationDropChecker
        implements IExternalSourceToRepresentationDropChecker {

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        return new CommunicationDropInsideRepresentationCheckerSwitch(newSemanticContainer).doSwitch(elementToDrop);
    }

    static class CommunicationDropInsideRepresentationCheckerSwitch extends UMLSwitch<CheckStatus> {

        private final EObject newSemanticContainer;

        CommunicationDropInsideRepresentationCheckerSwitch(EObject target) {
            super();
            this.newSemanticContainer = target;
        }

        @Override
        public CheckStatus caseComment(Comment comment) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Interaction)) {
                result = CheckStatus.no("Comment can only be drag and drop on an Interaction.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseConstraint(Constraint constraint) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Interaction)) {
                result = CheckStatus.no("Constraint can only be drag and drop on an Interaction.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseDurationObservation(DurationObservation durationObservation) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Interaction)) {
                result = CheckStatus.no("DurationObservation can only be drag and drop on an Interaction.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseLifeline(Lifeline lifeline) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Interaction)) {
                result = CheckStatus.no("Lifeline can only be drag and drop on an Interaction.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseProperty(Property property) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Lifeline)) {
                result = CheckStatus.no("Property can only be drag and drop on an Lifeline.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseTimeObservation(TimeObservation timeObservation) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Interaction)) {
                result = CheckStatus.no("TimeObservation can only be drag and drop on an Interaction.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseType(Type type) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Interaction)
                    && !(this.newSemanticContainer instanceof Lifeline)) {
                result = CheckStatus.no("Type can only be drag and drop on an Interaction or a Lifeline.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus defaultCase(EObject object) {
            return CheckStatus.no("DnD is not authorized.");
        }
    }

}
