/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.util.Collections;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.drop.DnDStatus;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.modify.ElementFeatureModifier;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Reception;
import org.eclipse.uml2.uml.Relationship;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Provides semantic drop behavior for elements (from the Model Explorer View)
 * to a Component diagram.
 *
 * @author <a href="mailto:gwendal.daniel@obeosoft.com">Gwendal Daniel</a>
 */
public class ComponentExternalSourceToRepresentationDropBehaviorProvider
        implements IExternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public DnDStatus drop(EObject droppedElement, EObject target, ECrossReferenceAdapter crossRef,
            IEditableChecker editableChecker) {
        return new ComponentDropInsideRepresentationBehaviorProviderSwitch(target, crossRef, editableChecker)
                .doSwitch(droppedElement);
    }

    static class ComponentDropInsideRepresentationBehaviorProviderSwitch extends UMLSwitch<DnDStatus> {

        private final EObject newSemanticContainer;

        private final ECrossReferenceAdapter crossRef;

        private final IEditableChecker editableChecker;

        ComponentDropInsideRepresentationBehaviorProviderSwitch(EObject target, ECrossReferenceAdapter crossRef,
                IEditableChecker editableChecker) {
            super();
            this.newSemanticContainer = target;
            this.crossRef = crossRef;
            this.editableChecker = editableChecker;
        }

        @Override
        public DnDStatus caseComment(Comment comment) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(comment));
            }
            return super.caseComment(comment);
        }

        @Override
        public DnDStatus caseComponent(Component component) {
            if (this.newSemanticContainer instanceof Component || this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(component));
            }
            return super.caseComponent(component);
        }

        @Override
        public DnDStatus caseConnector(Connector connector) {
            return DnDStatus.createNothingStatus(Set.of(connector));
        }

        @Override
        public DnDStatus caseConstraint(Constraint constraint) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(constraint));
            }
            return super.caseConstraint(constraint);
        }

        @Override
        public DnDStatus caseInterface(Interface interfaceElement) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(interfaceElement));
            }
            return super.caseInterface(interfaceElement);
        }

        @Override
        public DnDStatus caseOperation(Operation operation) {
            if (this.newSemanticContainer instanceof Interface) {
                return DnDStatus.createNothingStatus(Set.of(operation));
            }
            return super.caseOperation(operation);
        }

        @Override
        public DnDStatus casePackage(Package pack) {
            // This method handles both Package and Model instances
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(pack));
            }
            return super.casePackage(pack);
        }

        @Override
        public DnDStatus casePort(Port port) {
            if (this.newSemanticContainer instanceof Component || this.newSemanticContainer instanceof Property) {
                return DnDStatus.createNothingStatus(Set.of(port));
            }
            return super.casePort(port);
        }

        @Override
        public DnDStatus caseProperty(Property property) {
            if (this.newSemanticContainer instanceof Component || this.newSemanticContainer instanceof Interface
                    || (this.newSemanticContainer instanceof Property
                            && ((Property) this.newSemanticContainer).getType() != null)) {
                return DnDStatus.createNothingStatus(Set.of(property));
            }
            return super.caseProperty(property);
        }

        @Override
        public DnDStatus caseReception(Reception reception) {
            if (this.newSemanticContainer instanceof Interface) {
                return DnDStatus.createNothingStatus(Set.of(reception));
            }
            return super.caseReception(reception);
        }

        @Override
        public DnDStatus caseRelationship(Relationship relationship) {
            return DnDStatus.createNothingStatus(Set.of(relationship));
        }

        @Override
        public DnDStatus caseType(Type type) {
            if (this.newSemanticContainer instanceof Property targetProperty) {
                new ElementFeatureModifier(this.crossRef, this.editableChecker).setValue(targetProperty,
                        UMLPackage.eINSTANCE.getTypedElement_Type().getName(), type);
                return DnDStatus.createOKStatus(Collections.emptySet());
            }
            return super.caseType(type);
        }

        @Override
        public DnDStatus defaultCase(EObject object) {
            return DnDStatus.createFailingStatus("DnD is forbidden.", Collections.emptySet());
        }

    }

}
