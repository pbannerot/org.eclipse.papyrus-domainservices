/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.util.Collections;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.drop.DnDStatus;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.modify.ElementFeatureModifier;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.FunctionBehavior;
import org.eclipse.uml2.uml.InformationItem;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Relationship;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Drop behavior provider of a semantic drop (from Model Explorer view) to a
 * Composite Structure Diagram Element (or the root of the diagram itself).
 *
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 *
 */
public class CompositeStructureExternalSourceToRepresentationDropBehaviorProvider
        implements IExternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public DnDStatus drop(EObject droppedElement, EObject target, ECrossReferenceAdapter crossRef,
            IEditableChecker editableChecker) {
        return new StructureCompositeDropInsideRepresentationBehaviorProviderSwitch(target, crossRef, editableChecker)
                .doSwitch(droppedElement);
    }

    static class StructureCompositeDropInsideRepresentationBehaviorProviderSwitch extends UMLSwitch<DnDStatus> {

        private final EObject newSemanticContainer;

        private final ECrossReferenceAdapter crossRef;

        private final IEditableChecker editableChecker;

        StructureCompositeDropInsideRepresentationBehaviorProviderSwitch(EObject target,
                ECrossReferenceAdapter crossRef, IEditableChecker editableChecker) {
            super();
            this.newSemanticContainer = target;
            this.crossRef = crossRef;
            this.editableChecker = editableChecker;
        }

        @Override
        public DnDStatus caseActivity(Activity activity) {
            if (this.newSemanticContainer instanceof StructuredClassifier
                    || this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(activity));
            }
            return super.caseActivity(activity);
        }

        @Override
        public DnDStatus caseClass(Class clazz) {
            if (this.newSemanticContainer instanceof StructuredClassifier
                    || this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(clazz));
            }
            return super.caseClass(clazz);
        }

        @Override
        public DnDStatus caseCollaboration(Collaboration collaboration) {
            DnDStatus result = null;
            if (this.newSemanticContainer instanceof CollaborationUse) {
                CollaborationUse targetCollaborationUse = (CollaborationUse) this.newSemanticContainer;
                new ElementFeatureModifier(this.crossRef, this.editableChecker).setValue(targetCollaborationUse,
                        UMLPackage.eINSTANCE.getCollaborationUse_Type().getName(), collaboration);
                result = DnDStatus.createOKStatus(Set.of(collaboration));
            } else if (this.newSemanticContainer instanceof StructuredClassifier || //
                    this.newSemanticContainer instanceof Package) {
                result = DnDStatus.createNothingStatus(Set.of(collaboration));
            }
            return result;
        }

        @Override
        public DnDStatus caseCollaborationUse(CollaborationUse collaborationUse) {
            if (this.newSemanticContainer instanceof StructuredClassifier) {
                return DnDStatus.createNothingStatus(Set.of(collaborationUse));
            }
            return super.caseCollaborationUse(collaborationUse);
        }

        @Override
        public DnDStatus caseComment(Comment comment) {
            if (this.newSemanticContainer instanceof StructuredClassifier
                    || this.newSemanticContainer instanceof Package //
                    || this.newSemanticContainer instanceof Property) {
                return DnDStatus.createNothingStatus(Set.of(comment));
            }
            return super.caseComment(comment);
        }

        @Override
        public DnDStatus caseConnector(Connector connector) {
            return DnDStatus.createNothingStatus(Set.of(connector));
        }

        @Override
        public DnDStatus caseConstraint(Constraint constraint) {
            if (this.newSemanticContainer instanceof StructuredClassifier
                    || this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(constraint));
            }
            return super.caseConstraint(constraint);
        }

        @Override
        public DnDStatus caseFunctionBehavior(FunctionBehavior functionBehavior) {
            if (this.newSemanticContainer instanceof Class //
                    || this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(functionBehavior));
            }
            return super.caseFunctionBehavior(functionBehavior);
        }

        @Override
        public DnDStatus caseInformationItem(InformationItem informationItem) {
            if (this.newSemanticContainer instanceof Class //
                    || this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(informationItem));
            }
            return super.caseInformationItem(informationItem);
        }

        @Override
        public DnDStatus caseInteraction(Interaction interaction) {
            if (this.newSemanticContainer instanceof Class //
                    || this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(interaction));
            }
            return super.caseInteraction(interaction);
        }

        @Override
        public DnDStatus caseOpaqueBehavior(OpaqueBehavior opaqueBehavior) {
            if (this.newSemanticContainer instanceof Class || this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(opaqueBehavior));
            }
            return super.caseOpaqueBehavior(opaqueBehavior);
        }

        @Override
        public DnDStatus caseParameter(Parameter parameter) {
            if (this.newSemanticContainer instanceof StructuredClassifier
                    && !(this.newSemanticContainer instanceof Collaboration
                            // Use eClass to check exact type
                            || this.newSemanticContainer.eClass().equals(UMLPackage.eINSTANCE.getClass_()))) {
                return DnDStatus.createNothingStatus(Set.of(parameter));
            }
            return super.caseParameter(parameter);
        }

        @Override
        public DnDStatus caseProperty(Property property) {
            if (this.newSemanticContainer instanceof Classifier || (this.newSemanticContainer instanceof Property
                    && ((Property) this.newSemanticContainer).getType() != null)) {
                return DnDStatus.createNothingStatus(Set.of(property));
            }
            return super.caseProperty(property);
        }

        @Override
        public DnDStatus caseRelationship(Relationship relationship) {
            return DnDStatus.createNothingStatus(Set.of(relationship));
        }

        @Override
        public DnDStatus caseStateMachine(StateMachine stateMachine) {
            if (this.newSemanticContainer instanceof Class //
                    || this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(stateMachine));
            }
            return super.caseStateMachine(stateMachine);
        }

        @Override
        public DnDStatus caseType(Type type) {
            if (this.newSemanticContainer instanceof Property) {
                Property targetProperty = (Property) this.newSemanticContainer;
                new ElementFeatureModifier(this.crossRef, this.editableChecker).setValue(targetProperty,
                        UMLPackage.eINSTANCE.getTypedElement_Type().getName(), type);
                return DnDStatus.createOKStatus(Collections.emptySet());
            }
            return super.caseType(type);
        }

        @Override
        public DnDStatus defaultCase(EObject object) {
            return DnDStatus.createFailingStatus("DnD is forbidden.", Collections.emptySet());
        }

    }

}
