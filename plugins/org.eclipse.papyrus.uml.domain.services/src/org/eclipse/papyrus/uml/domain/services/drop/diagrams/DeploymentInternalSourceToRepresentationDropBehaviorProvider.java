/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.util.Optional;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.drop.IInternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.modify.ElementFeatureModifier;
import org.eclipse.papyrus.uml.domain.services.modify.IFeatureModifier;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Node;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Provides drop behavior for diagram element in the Deployment diagram.
 *
 * @author <a href="mailto:florian.barbin@obeo.fr">Florian Barbin</a>
 */
public class DeploymentInternalSourceToRepresentationDropBehaviorProvider
implements IInternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public Status drop(EObject droppedElement, EObject oldContainer, EObject newContainer,
            ECrossReferenceAdapter crossRef, IEditableChecker editableChecker) {
        return new DeploymentDropOutsideRepresentationBehaviorProviderSwitch(oldContainer, newContainer, crossRef,
                editableChecker).doSwitch(droppedElement);
    }

    static class DeploymentDropOutsideRepresentationBehaviorProviderSwitch extends UMLSwitch<Status> {

        private static final String UNSUPPORTED_DROP_CONTAINER = "Unsupported drop container.";

        private final EObject oldContainer;

        private final EObject newContainer;

        private final ECrossReferenceAdapter crossRef;

        private final IEditableChecker editableChecker;

        /**
         * Initializes the switch.
         *
         * @param oldContainer
         *                        the old container of the element being dropped
         * @param newContainer
         *                        the new container of the element being dropped
         * @param crossRef
         *                        the cross referencer
         * @param editableChecker
         *                        the checker
         */
        DeploymentDropOutsideRepresentationBehaviorProviderSwitch(EObject oldContainer, EObject newContainer,
                ECrossReferenceAdapter crossRef, IEditableChecker editableChecker) {
            super();
            this.oldContainer = oldContainer;
            this.newContainer = newContainer;
            this.crossRef = crossRef;
            this.editableChecker = editableChecker;
        }

        /**
         * Handle the {@link Artifact} case.
         * 
         * @param artifact
         *                 the dropped {@link Artifact}.
         * @return OK or Failing status according to the complete D&D.
         */
        @Override
        public Status caseArtifact(Artifact artifact) {
            Status dropStatus = Status.createFailingStatus(UNSUPPORTED_DROP_CONTAINER);
            if (this.oldContainer != this.newContainer) {
                Optional<EReference> optionalContainmentFeature = this.getArtifactContainmentFeature(this.newContainer);
                IFeatureModifier modifier = new ElementFeatureModifier(this.crossRef, this.editableChecker);
                if (optionalContainmentFeature.isPresent()) {
                    String refName = artifact.eContainmentFeature().getName();
                    dropStatus = modifier.removeValue(this.oldContainer, refName, artifact);
                    if (State.DONE == dropStatus.getState()) {
                        dropStatus = modifier.addValue(this.newContainer, optionalContainmentFeature.get().getName(),
                                artifact);
                    }
                }
            }
            return dropStatus;
        }

        private Optional<EReference> getArtifactContainmentFeature(EObject container) {
            Optional<EReference> reference;
            if (container instanceof Package pkg) {
                reference = Optional.of(UMLPackage.eINSTANCE.getPackage_PackagedElement());
            } else if (container instanceof org.eclipse.uml2.uml.Class clazz) {
                reference = Optional.of(UMLPackage.eINSTANCE.getClass_NestedClassifier());
            } else if (container instanceof Artifact artifact) {
                reference = Optional.of(UMLPackage.eINSTANCE.getArtifact_NestedArtifact());
            } else {
                reference = Optional.empty();
            }
            return reference;
        }

        /**
         * Default Behavior : UML element can be D&D by using the same reference
         * containment.
         *
         * @see org.eclipse.uml2.uml.util.UMLSwitch#caseElement(org.eclipse.uml2.uml.Element)
         *
         * @param droppedElement
         *                       the element to drop
         * @return OK or Failing status according to the complete D&D.
         */
        @Override
        public Status caseElement(Element droppedElement) {
            Status dropStatus = Status.createFailingStatus(UNSUPPORTED_DROP_CONTAINER);
            IFeatureModifier modifier = new ElementFeatureModifier(this.crossRef, this.editableChecker);
            if (this.oldContainer != this.newContainer) {
                String refName = droppedElement.eContainmentFeature().getName();
                if (this.oldContainer.eClass().getEStructuralFeature(refName) != null
                        && this.newContainer.eClass().getEStructuralFeature(refName) != null) {
                    dropStatus = modifier.removeValue(this.oldContainer, refName, droppedElement);
                    if (State.DONE == dropStatus.getState()) {
                        dropStatus = modifier.addValue(this.newContainer, refName, droppedElement);
                    }
                }
            }
            return dropStatus;
        }

        /**
         * Handle the {@link Node} case.
         * 
         * @param node
         *             the dropped {@link Node}.
         * @return OK or Failing status according to the complete D&D.
         */
        @Override
        public Status caseNode(Node node) {
            Status dropStatus = Status.createFailingStatus(UNSUPPORTED_DROP_CONTAINER);
            if (this.oldContainer != this.newContainer) {
                Optional<EReference> optionalContainmentFeature = this.getNodeContainmentFeature(this.newContainer);
                IFeatureModifier modifier = new ElementFeatureModifier(this.crossRef, this.editableChecker);
                if (optionalContainmentFeature.isPresent()) {
                    String refName = node.eContainmentFeature().getName();
                    dropStatus = modifier.removeValue(this.oldContainer, refName, node);
                    if (State.DONE == dropStatus.getState()) {
                        dropStatus = modifier.addValue(this.newContainer, optionalContainmentFeature.get().getName(), node);
                    }
                }
            }
            return dropStatus;
        }

        private Optional<EReference> getNodeContainmentFeature(EObject container) {
            Optional<EReference> reference;
            if (container instanceof Package pkg) {
                reference = Optional.of(UMLPackage.eINSTANCE.getPackage_PackagedElement());
            } else if (container instanceof Node node) {
                reference = Optional.of(UMLPackage.eINSTANCE.getNode_NestedNode());
            } else {
                reference = Optional.empty();
            }
            return reference;
        }

    }

}
