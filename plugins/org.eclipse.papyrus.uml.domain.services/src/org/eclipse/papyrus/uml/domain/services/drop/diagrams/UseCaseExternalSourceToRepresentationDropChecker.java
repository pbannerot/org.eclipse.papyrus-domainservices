/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.util.UMLSwitch;

public class UseCaseExternalSourceToRepresentationDropChecker implements IExternalSourceToRepresentationDropChecker {

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        return new UseCaseDropInsideRepresentationCheckerSwitch(newSemanticContainer).doSwitch(elementToDrop);
    }

    static class UseCaseDropInsideRepresentationCheckerSwitch extends UMLSwitch<CheckStatus> {

        private final EObject newSemanticContainer;

        UseCaseDropInsideRepresentationCheckerSwitch(EObject target) {
            super();
            this.newSemanticContainer = target;
        }

        @Override
        public CheckStatus caseActivity(Activity activity) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Package)) {
                result = CheckStatus.no("Activity can only be drag and drop on Package.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseActor(Actor actor) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Package)) {
                result = CheckStatus.no("Actor can only be drag and drop on Package Elements.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseClass(Class clazz) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Package)) {
                result = CheckStatus.no("Class can only be drag and drop on Package.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseComment(Comment comment) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Package)
                    && !(this.newSemanticContainer instanceof org.eclipse.uml2.uml.Class)) {
                result = CheckStatus.no("Comment can only be drag and drop on Package or subject Elements.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseComponent(Component component) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Package)) {
                result = CheckStatus.no("Component can only be drag and drop on Package.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseConstraint(Constraint constraint) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Package)
                    && !(this.newSemanticContainer instanceof org.eclipse.uml2.uml.Class)) {
                result = CheckStatus.no("Constraint can only be drag and drop on Package or subject Elements.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseInteraction(Interaction interaction) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Package)) {
                result = CheckStatus.no("Interaction can only be drag and drop on Package.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus casePackage(Package pack) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Package)) {
                result = CheckStatus.no("Package can only be drag and drop on Package Elements.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseStateMachine(StateMachine stateMachine) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Package)) {
                result = CheckStatus.no("State Machine can only be drag and drop on Package.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseUseCase(UseCase useCase) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Package)
                    && !(this.newSemanticContainer instanceof org.eclipse.uml2.uml.Class)) {
                result = CheckStatus.no("Use Case can only be drag and drop on Package or subject Elements.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus defaultCase(EObject object) {
            return CheckStatus.no("DnD is not authorized.");
        }
    }
}
