/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.internal.helpers;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.uml2.common.util.CacheAdapter;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ExecutionOccurrenceSpecification;
import org.eclipse.uml2.uml.ExecutionSpecification;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.OccurrenceSpecification;
import org.eclipse.uml2.uml.TimeConstraint;
import org.eclipse.uml2.uml.TimeObservation;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * This helper provides some utils method on {@link OccurrenceSpecification}.
 * 
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 */
public class OccurrenceSpecificationHelper {

    /**
     * Get interaction from a given {@link Element}.
     * 
     * @param element
     *                the element attached to an interaction
     * @return its matching interaction.
     */
    public static Interaction getInteraction(Element element) {
        Interaction result = null;

        for (Element next = element; next != null && result == null; next = next.getOwner()) {
            if (next instanceof Interaction) {
                result = (Interaction) next;
            }
        }

        return result;
    }

    /**
     * Get time constraints in the contextual {@code interaction} that constrain
     * only the {@code constrained} element and no others.
     *
     * @param interaction
     *                    the contextual interaction
     * @param constrained
     *                    the constrained element
     *
     * @return its unique time constraints
     */
    public static Stream<TimeConstraint> getTimeConstraints(Interaction interaction, Element constrained) {
        if (interaction == null) {
            return Stream.empty();
        }

        return CacheAdapter.getInstance().getNonNavigableInverseReferences(constrained).stream().filter(
                setting -> setting.getEStructuralFeature() == UMLPackage.Literals.CONSTRAINT__CONSTRAINED_ELEMENT)
                .map(setting -> (Constraint) setting.getEObject()).filter(TimeConstraint.class::isInstance)
                .filter(c -> c.getConstrainedElements().size() == 1).filter(c -> getInteraction(c) == interaction)
                .map(TimeConstraint.class::cast);
    }

    /**
     * Get time observations in the contextual {@code interaction} that reference
     * the given {@code observed} element.
     *
     * @param interaction
     *                    the contextual interaction
     * @param observed
     *                    the observed element
     *
     * @return its unique time constraints
     */
    public static Stream<TimeObservation> getTimeObservations(Interaction interaction, Element observed) {
        if (interaction == null) {
            return Stream.empty();
        }

        // These observations are contained by packages, so the interaction context
        // isn't
        // actually useful. It is specified for API consistency with other cases
        return CacheAdapter.getInstance().getNonNavigableInverseReferences(observed).stream()
                .filter(setting -> setting.getEStructuralFeature() == UMLPackage.Literals.TIME_OBSERVATION__EVENT)
                .map(setting -> (TimeObservation) setting.getEObject());
    }

    /**
     * <pre>
     * Check that given {@link OccurrenceSpecification} should be destroyed along with {@link ExecutionSpecification} which references it.
     * It should be destroyed in case:
     * It is of type {@link ExecutionOccurrenceSpecification} (since the opposite reference
     *   'ExecutionOccurrenceSpecification::execution[1]' which designates given {@link ExecutionSpecification} is mandatory).
     *   or
     * It is not used by another element.
     * </pre>
     *
     * @param es
     *           {@link ExecutionSpecification} which references
     *           {@link OccurrenceSpecification} (by means of #start/#finish
     *           references)
     * @param os
     *           start or finish {@link OccurrenceSpecification} which defines the
     *           duration of {@link ExecutionSpecification}
     * @return true in case {@link OccurrenceSpecification} should be destroyed
     */
    public static boolean shouldDestroyOccurrenceSpecification(ExecutionSpecification es, OccurrenceSpecification os,
            ECrossReferenceAdapter crossReferenceAdapter) {
        return os instanceof ExecutionOccurrenceSpecification
                || (os != null && UMLService.isOnlyUsage(os, es, crossReferenceAdapter));
    }

    /**
     * Returns the element enclosing the {@code occurrenceSpecification}.
     * <p>
     * This element can be:
     * <ul>
     * <li>An {@link ExecutionSpecification} if the {@code occurrenceSpecification}
     * is positioned between the {@link ExecutionSpecification#getStart()} and
     * {@link ExecutionSpecification#getFinish()} occurrences in the enclosing
     * {@link Interaction#getFragments()}
     * <li>It's first covered {@link Lifeline} otherwise (see
     * {@link OccurrenceSpecification#getCovereds()})
     * 
     * @param occurrenceSpecification
     *                                the {@link OccurrenceSpecification} to
     *                                retrieve the enclosing element from
     * @return the element enclosing the {@code occurrenceSpecification}
     * 
     * @throws NullPointerException
     *                              if {@code occurrenceSpecification} is
     *                              {@code null}
     */
    public static NamedElement findEnclosingElement(OccurrenceSpecification occurrenceSpecification) {
        Objects.requireNonNull(occurrenceSpecification);
        Lifeline coveredLifeline = occurrenceSpecification.getCovered();
        NamedElement enclosingElement = null;
        if (coveredLifeline != null) {
            enclosingElement = coveredLifeline;

            /*
             * Get all the enclosing fragments that cover the current Lifeline.
             */
            // @formatter:off
            final List<InteractionFragment> fragments = occurrenceSpecification.getEnclosingInteraction().getFragments()
                    .stream()
                    .filter(InteractionFragment.class::isInstance)
                    .map(InteractionFragment.class::cast)
                    .filter(i -> i.getCovereds().contains(coveredLifeline))
                    .collect(Collectors.toList());
                    // @formatter:on

            ExecutionSpecification enclosingExecutionSpecification = null;
            for (InteractionFragment fragment : fragments) {
                if (fragment == occurrenceSpecification) {
                    break;
                } else if (fragment instanceof ExecutionSpecification) {
                    enclosingExecutionSpecification = (ExecutionSpecification) fragment;
                } else if (fragment instanceof OccurrenceSpecification) {
                    OccurrenceSpecification occurrenceSpecificationFragment = (OccurrenceSpecification) fragment;
                    if (enclosingExecutionSpecification != null && Objects
                            .equals(enclosingExecutionSpecification.getFinish(), occurrenceSpecificationFragment)) {
                        enclosingExecutionSpecification = null;
                    }
                }
            }
            if (enclosingExecutionSpecification != null) {
                // Return the enclosing ExecutionSpecification if we found one, otherwise return
                // the enclosing Lifeline
                enclosingElement = enclosingExecutionSpecification;
            }
        }
        return enclosingElement;
    }
}
