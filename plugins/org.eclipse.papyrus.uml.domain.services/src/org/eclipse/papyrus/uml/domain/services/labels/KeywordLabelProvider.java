/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels;

import static java.util.stream.Collectors.joining;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Abstraction;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.DeploymentSpecification;
import org.eclipse.uml2.uml.Device;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.ExecutionEnvironment;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.FunctionBehavior;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.InformationItem;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.ProtocolStateMachine;
import org.eclipse.uml2.uml.Realization;
import org.eclipse.uml2.uml.Reception;
import org.eclipse.uml2.uml.Signal;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.VisibilityKind;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Computes the prefix representing all keywords applied to a given element.
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 *
 */
public class KeywordLabelProvider implements Function<EObject, String> {

    @Override
    public String apply(EObject t) {
        if (t != null) {
            List<String> keywords = new KeywordProviderSwitch().doSwitch(t);
            if (keywords != null) {
                return keywords.stream()//
                        .collect(joining(UMLCharacters.COMMA + UMLCharacters.SPACE, UMLCharacters.ST_LEFT,
                                UMLCharacters.ST_RIGHT));
            }
        }
        return null;
    }

    static class KeywordProviderSwitch extends UMLSwitch<List<String>> {

        @Override
        public List<String> caseAbstraction(Abstraction object) {
            if (!(object instanceof Realization)) {
                return List.of("abstraction");
            }
            return super.caseAbstraction(object);
        }

        @Override
        public List<String> caseActivity(Activity activity) {
            List<String> keywords = new ArrayList<>();
            keywords.add("activity");
            if (activity.isSingleExecution()) {
                keywords.add("singleExecution");
            }
            return keywords;
        }

        @Override
        public List<String> caseArtifact(Artifact artifact) {
            return List.of("artifact");
        }

        @Override
        public List<String> caseCollaboration(Collaboration object) {
            return List.of("collaboration");
        }

        @Override
        public List<String> caseComponent(Component object) {
            return List.of("component");
        }

        @Override
        public List<String> caseDataType(DataType dataType) {
            return List.of("dataType");
        }

        @Override
        public List<String> caseDeployment(Deployment deployment) {
            return List.of("deploy");
        }

        @Override
        public List<String> caseDeploymentSpecification(DeploymentSpecification deploymentSpecification) {
            return List.of("deployment spec");
        }

        @Override
        public List<String> caseDevice(Device object) {
            return List.of("device");
        }

        @Override
        public List<String> caseEnumeration(Enumeration enumeration) {
            return List.of("enumeration");
        }

        @Override
        public List<String> caseExecutionEnvironment(ExecutionEnvironment executionEnvironment) {
            return List.of("executionEnvironment");
        }

        @Override
        public List<String> caseExtend(Extend extend) {
            return List.of("extend");
        }

        @Override
        public List<String> caseFunctionBehavior(FunctionBehavior functionBehavior) {
            return List.of("functionBehavior");
        }

        @Override
        public List<String> caseInclude(Include object) {
            return List.of("include");
        }

        @Override
        public List<String> caseInformationFlow(InformationFlow flow) {
            return List.of("flow");
        }

        @Override
        public List<String> caseInformationItem(InformationItem object) {
            return List.of("information");
        }

        @Override
        public List<String> caseInteraction(Interaction interaction) {
            return List.of("interaction");
        }

        @Override
        public List<String> caseInterface(Interface object) {
            return List.of("interface");
        }

        @Override
        public List<String> caseManifestation(Manifestation object) {
            return List.of("manifest");
        }

        @Override
        public List<String> caseOpaqueBehavior(OpaqueBehavior opaqueBehavior) {
            return List.of("opaqueBehavior");
        }

        @Override
        public List<String> casePackageImport(PackageImport object) {
            if (object.getVisibility() == VisibilityKind.PUBLIC_LITERAL) {
                return List.of("import");
            } else {
                return List.of("access");
            }
        }

        @Override
        public List<String> casePackageMerge(PackageMerge object) {
            return List.of("merge");
        }

        @Override
        public List<String> casePrimitiveType(PrimitiveType object) {
            return List.of("primitive");
        }

        @Override
        public List<String> caseProfile(Profile object) {
            return List.of("profile");
        }

        @Override
        public List<String> caseProtocolStateMachine(ProtocolStateMachine protocolStateMachine) {
            return List.of("protocol");
        }

        @Override
        public List<String> caseReception(Reception object) {
            return List.of("signal");
        }

        @Override
        public List<String> caseSignal(Signal signal) {
            return List.of("signal");
        }

        @Override
        public List<String> caseStateMachine(StateMachine stateMachine) {
            return List.of("stateMachine");
        }

        @Override
        public List<String> caseStereotype(Stereotype stereotype) {
            return List.of("stereotype");
        }

        @Override
        public List<String> caseSubstitution(Substitution object) {
            return List.of("substitute");
        }

        @Override
        public List<String> caseUsage(Usage object) {
            return List.of("use");
        }
    }
}
