/*****************************************************************************
 * Copyright (c) 2024 CEA LIST, Artal Technologies
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Titouan BOUETE-GIRAUD (Artal Technologies) - titouan.bouete-giraud@artal.fr - Issue 17
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.labels.domains;

import static org.eclipse.papyrus.uml.domain.services.labels.LabelUtils.getNonNullString;

import java.util.Objects;
import java.util.stream.Collectors;

import org.eclipse.papyrus.uml.domain.services.labels.INamedElementNameProvider;
import org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.Reception;

/**
 * Helper for {@link Reception} label.
 * 
 * @author Titouan BOUETE-GIRAUD
 */

public class ReceptionLabelHelper {
    
    private final ParameterLabelHelper parameterLabelHelper;
    private final VisibilityLabelHelper visibilityLabelHelper;
    private final INamedElementNameProvider namedElementNameProvider;
    
    public ReceptionLabelHelper(ParameterLabelHelper parameterLabelHelper, VisibilityLabelHelper visibilityLabelHelper,
            INamedElementNameProvider namedElementNameProvider) {
        this.parameterLabelHelper = Objects.requireNonNull(parameterLabelHelper);
        this.visibilityLabelHelper = Objects.requireNonNull(visibilityLabelHelper);
        this.namedElementNameProvider = Objects.requireNonNull(namedElementNameProvider);
    }

    public String getLabel(Reception reception) {
        String receptionLabel = getNonNullString(visibilityLabelHelper.getVisibilityAsSign(reception))
                + UMLCharacters.SPACE;
        String parameters = getInnerParameters(reception);

        return receptionLabel + this.namedElementNameProvider.getName(reception) + parameters;
    }
    
    private String getInnerParameters(Reception reception) {
        //@formatter:off
        return reception.getOwnedParameters().stream()
                .filter(p -> !ParameterDirectionKind.RETURN_LITERAL.equals(p.getDirection()))
                .map(this::computeOperationParameterLabel)
                .collect(Collectors.joining(UMLCharacters.COMMA + UMLCharacters.SPACE,
                        UMLCharacters.OPEN_PARENTHESE, UMLCharacters.CLOSE_PARENTHESE));
        //@formatter:on
    }
    
    private String computeOperationParameterLabel(Parameter parameter) {
        return this.parameterLabelHelper.getLabel(parameter);
    }
    
}
