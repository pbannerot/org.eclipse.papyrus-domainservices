/*****************************************************************************
 * Copyright (c) 2008, 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Chokri Mraidha (CEA LIST) Chokri.Mraidha@cea.fr - Initial API and implementation
 *  Patrick Tessier (CEA LIST) Patrick.Tessier@cea.fr - modification
 *
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.profile;

import java.util.Optional;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EcoreFactory;

/**
 * Class that defines various information about a profile definition (author,
 * version, etc.).<br/>
 * Copied from
 * org.eclipse.papyrus.uml.tools.profile.definition.PapyrusDefinitionAnnotation
 * 
 * @author Chokri Mraidha
 */
public class ProfileDefinition {
    /** undefined PapyrusDefinitionAnnotation. */
    public static final ProfileDefinition UNDEFINED_ANNOTATION = new ProfileDefinition(ProfileVersion.EMPTY_VERSION,
            "<undefined>", "", "", "<undefined>");

    /** version of the definition */
    private ProfileVersion version = ProfileVersion.EMPTY_VERSION;

    /** Comment of the definition */
    private String comment = "";

    /** Copyright of the definition */
    private String copyright = "";

    /** date of the definition */
    private String date = "";

    /** author of the definition */
    private String author = "";

    /**
     * Creates a new PapyrusDefinitionAnnotation.
     *
     * @param version
     *                  the version of the definition
     * @param comment
     *                  the comment associated to this definition
     * @param copyright
     *                  the copyright of this definition
     * @param date
     *                  the date this definition was generated
     * @param the
     *                  author responsible of this definition
     */
    public ProfileDefinition(ProfileVersion version, String comment, String copyright, String date, String author) {
        this.version = version;
        this.comment = comment;
        this.copyright = copyright;
        this.author = author;
        this.date = date;
    }

    /**
     * Creates a EAnnotation from the given configuration.
     *
     * @return the eAnnotation corresponding to this configuration
     */
    public EAnnotation convertToEAnnotation() {
        EAnnotation annotation = EcoreFactory.eINSTANCE.createEAnnotation();
        // set various values (default if elements are null)
        annotation.setSource(ProfileAttributeConstants.PROFILE_EANNOTATION_SOURCE);
        annotation.getDetails().put(ProfileAttributeConstants.PROFILE_VERSION_KEY, version.toString());
        annotation.getDetails().put(ProfileAttributeConstants.PROFILE_COMMENT_KEY, comment);
        annotation.getDetails().put(ProfileAttributeConstants.PROFILE_COPYRIGHT_KEY, copyright);
        annotation.getDetails().put(ProfileAttributeConstants.PROFILE_DATE_KEY, date);
        annotation.getDetails().put(ProfileAttributeConstants.PROFILE_AUTHOR_KEY, author);
        return annotation;
    }

    /**
     * Return the PapyrusDefinitionAnnotation corresponding to the given
     * EAnnotation.
     *
     * @param annotation
     *                   the annotation to parse
     * @return a image of the given annotation, with default values if needed.
     */
    public static ProfileDefinition parseEAnnotation(EAnnotation annotation) {
        final String versionValue = annotation.getDetails().get(ProfileAttributeConstants.PROFILE_VERSION_KEY);
        ProfileVersion version;
        try {
            version = ProfileVersion.parseVersion(versionValue);
        } catch (IllegalArgumentException e) {
            version = ProfileVersion.EMPTY_VERSION;
        }
        final String comment = Optional.of(annotation.getDetails().get(ProfileAttributeConstants.PROFILE_COMMENT_KEY))
                .orElse("");
        final String copyright = Optional
                .of(annotation.getDetails().get(ProfileAttributeConstants.PROFILE_COPYRIGHT_KEY)).orElse("");
        final String date = Optional.of(annotation.getDetails().get(ProfileAttributeConstants.PROFILE_DATE_KEY))
                .orElse("");
        final String author = Optional.of(annotation.getDetails().get(ProfileAttributeConstants.PROFILE_AUTHOR_KEY))
                .orElse("");
        return new ProfileDefinition(version, comment, copyright, date, author);
    }

    /**
     * Returns the version of the definition of the profile.
     *
     * @return the version of the definition of the profile
     */
    public ProfileVersion getVersion() {
        return version;
    }

    /**
     * Returns the comment associated to the definition of the profile.
     *
     * @return the comment associated to the definition of the profile
     */
    public String getComment() {
        return comment;
    }

    /**
     * Returns the copyright associated to the definition of the profile.
     *
     * @return the copyright associated to the definition of the profile
     */
    public String getCopyright() {
        return copyright;
    }

    /**
     * Returns the date associated to the definition of the profile.
     *
     * @return the date associated to the definition of the profile
     */
    public String getDate() {
        return date;
    }

    /**
     * Returns the author responsible to the definition of the profile.
     *
     * @return the author responsible to the definition of the profile
     */
    public String getAuthor() {
        return author;
    }
}
