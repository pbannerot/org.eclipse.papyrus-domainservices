/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.properties;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.papyrus.uml.domain.services.EMFUtils;

/**
 * Service that handles reference types services.
 *
 * @author <a href="mailto:jerome.gout@obeosoft.fr">Jerome Gout</a>
 */
public class PropertiesUMLReferenceTypeServices {
    
    // key of feature annotation details map to retrieve the redefined type
    private static final String E_TYPE = "eType";

    // type redefinition root annotation source value
    private static final String DUPLICATES = "duplicates";

    private static final String KEY_ELEMENTS_SEPARATOR = ":";
    
    private final ILogger logger;

    // for instance <"DurationConstraint:specification", "uml::DurationInterval">
    private Map<String, String> redefinedReferenceTypes;

    public PropertiesUMLReferenceTypeServices(ILogger logger) {
        this.logger = logger;
    }

    /**
     * Initializes the internal type redefinition registry of the given package.
     * @param uml2EPackage the package in which the type redefinitions are searched.
     */
    public void initRedefinedTypes(EPackage uml2EPackage) {
        redefinedReferenceTypes = new HashMap<>();
        List<EClass> classes = EMFUtils.allContainedObjectOfType(uml2EPackage, EClass.class).filter(e -> !e.isInterface() && !e.isAbstract()).toList();
        classes.stream().forEach(c -> {
            this.registerEntry(c, c);
            // is one of its super types containing an annotation?
            for (EClass superC : c.getEAllSuperTypes()) {
                this.registerEntry(c, superC);
            }
        });
    }
    
    private void registerEntry(EClass clazz, EClass annotationContainerClass) {
        List<EAnnotation> annotations = this.getRedefinedAnnotations(annotationContainerClass);
        for (EAnnotation anno : annotations) {
            String overridenType = anno.getDetails().get(E_TYPE);
            String key = clazz.getName() + KEY_ELEMENTS_SEPARATOR + anno.getSource();
            if (!this.redefinedReferenceTypes.containsKey(key)) {
                this.redefinedReferenceTypes.put(key, overridenType);
            }
        }
    }

    private List<EAnnotation> getRedefinedAnnotations(EClass clazz) {
        return clazz.getEAnnotations().stream().filter(anno -> DUPLICATES.equals(anno.getSource())).flatMap(a -> a.getEAnnotations().stream()).filter(a -> a.getDetails().get(E_TYPE) != null)
                .toList();
    }

    /**
     * Returns the reference redefined type name if it has been redefined in UML model.
     *
     * @param clazz
     *            the class owning the reference
     * @param featureName
     *            the reference name
     * @return an Optional describing the actual reference type name, if the type of this reference has been redefined
     *         otherwise returns an empty Optional.
     */
    private Optional<String> findRedefinedReferenceType(EClass clazz, String featureName) {
        return Optional.ofNullable(this.redefinedReferenceTypes.get(clazz.getName() + KEY_ELEMENTS_SEPARATOR + featureName));
    }
    
    
    /**
     * Return the qualified name of the type of the given feature of the given element.
     * @param self
     *            the current selected element owning the feature
     * @param featureName
     *            the name of the feature
     * @return the qualified name of the feature type.
     */
    public String getFeatureTypeQualifiedName(EObject self, String featureName) {
        if (redefinedReferenceTypes == null) {
            throw new IllegalStateException("The 'PropertiesUMLRedefinedReferenceTypeServices::init()' method should be called before requesting reference type.");
        }
        Optional<String> optionalType = findRedefinedReferenceType(self.eClass(), featureName);
        if (optionalType.isEmpty()) {
            String res = "";
            var feature = self.eClass().getEStructuralFeature(featureName);
            if (feature != null) {
                var type = feature.getEType();
                if (type != null) {
                    res = type.getEPackage().getName() + "::" + type.getName();
                } else {
                    logger.log(MessageFormat.format("Unable to get the type of feature ''{0}'' in ''{1}''", featureName, self.eClass().getName()), ILogger.ILogLevel.ERROR);
                }
            } else {
                logger.log(MessageFormat.format("''{0}'' is not a feature of class ''{1}''", featureName, self.eClass().getName()), ILogger.ILogLevel.ERROR);
            }
            return res;
        } else {
            return optionalType.get();
        }
    }
}
