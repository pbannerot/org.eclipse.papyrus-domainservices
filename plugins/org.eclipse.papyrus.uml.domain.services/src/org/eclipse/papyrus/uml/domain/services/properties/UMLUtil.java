/*******************************************************************************
 * Copyright (c) 2010, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.properties;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Stereotype;

/**
 * UML toolings. (extrated from org.eclipse.papyrus.uml.tools.utils.UMLUtil).
 * 
 * @author Camille Letavernier
 * @author <a href="mailto:jerome.gout@obeosoft.com">Jerome Gout</a>
 */
public class UMLUtil {

    /**
     * Search the given stereotype (by name) on the given UML Element. If the search
     * is not strict, the name may be the qualified name of a super-stereotype of
     * some applied stereotype.
     *
     * @param umlElement
     *                       The UML Element on which the stereotype is applied
     * @param stereotypeName
     *                       The qualified name of the stereotype
     * @param strict
     *                       If set to true, only a stereotype matching the exact
     *                       qualified name will be returned. Otherwise, any subtype
     *                       of the given stereotype may be returned. Note that if
     *                       more than one stereotype is a subtype of the given
     *                       stereotype, the first matching stereotype is returned.
     * @return The first matching stereotype, or null if none was found
     */
    public static Stereotype getAppliedStereotype(Element umlElement, String stereotypeName, boolean strict) {
        Stereotype res = null;
        if (umlElement != null && stereotypeName != null) {
            res = umlElement.getAppliedStereotype(stereotypeName);
            if (!strict && res == null) {
                // now check whether one of the applied stereotypes has the requested super
                // stereotype
                for (Stereotype subStereotype : umlElement.getAppliedStereotypes()) {
                    for (Stereotype superStereotype : getAllSuperStereotypes(subStereotype)) {
                        if (stereotypeName.equals(superStereotype.getQualifiedName())) {
                            // return applied stereotype whose super-stereotype matches given name
                            return subStereotype;
                        }
                    }
                }
            }
        }
        return res;
    }

    /**
     * Returns a collection of all super stereotypes of the given stereotype
     * (Including itself).
     *
     * @param stereotype
     * @return A collection of all super stereotypes
     */
    public static Collection<Stereotype> getAllSuperStereotypes(Stereotype stereotype) {
        Set<Stereotype> result = new HashSet<Stereotype>();
        if (stereotype != null) {
            getAllSuperStereotypes(stereotype, result);
        }
        return result;
    }

    private static void getAllSuperStereotypes(Stereotype stereotype, Set<Stereotype> result) {
        result.add(stereotype);
        for (Classifier superClassifier : stereotype.getGenerals()) {
            if (superClassifier instanceof Stereotype && !result.contains(superClassifier)) {
                getAllSuperStereotypes((Stereotype) superClassifier, result);
            }
        }
    }
}
