/*******************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.properties;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * Representation of the version number for a profile. (copied from
 * org.eclipse.papyrus.uml.tools.profile.definition.Version)
 * 
 * @author <a href="mailto:jerome.gout@obeosoft.com">Jerome Gout</a> (not original author)
 */
public class Version {

    /**
     * The empty version "0.0.0". Equivalent to calling
     * <code>new Version(0,0,0)</code>.
     */
    public static final Version EMPTY_VERSION = new Version(0, 0, 0);

    /** separator for the version string. */
    private static final String SEPARATOR = ".";

    /** major version number. */
    protected int major;

    /** minor version number. */
    protected int minor;

    /** micro version number. */
    protected int micro;


    /**
     * Creates a new Version.
     *
     * @param major
     *              the major version value (should be positive)
     * @param minor
     *              the minor version value (should be positive)
     * @param micro
     *              the micro version value (should be positive)
     */
    public Version(int major, int minor, int micro) {
        this.major = major;
        this.minor = minor;
        this.micro = micro;
    }

    /**
     * Creates a new Version, parsing a string value.
     *
     * @param value
     *              the string representing the version
     */
    public Version(String value) throws IllegalArgumentException {
        try {
            StringTokenizer st = new StringTokenizer(value, SEPARATOR, true);
            major = Integer.parseInt(st.nextToken());

            if (st.hasMoreTokens()) {
                st.nextToken(); // consume delimiter
                minor = Integer.parseInt(st.nextToken());

                if (st.hasMoreTokens()) {
                    st.nextToken(); // consume delimiter
                    micro = Integer.parseInt(st.nextToken());

                    if (st.hasMoreTokens()) {
                        throw new IllegalArgumentException("invalid format");
                    }
                }
            }
        } catch (NoSuchElementException e) {
            throw new IllegalArgumentException("invalid format");
        }
    }

    /**
     * Returns the major version number.
     *
     * @return The major version number
     */
    public int getMajor() {
        return major;
    }

    /**
     * Returns the minor version number.
     *
     * @return The minor version number
     */
    public int getMinor() {
        return minor;
    }

    /**
     * Returns the micro version number.
     *
     * @return The micro version number
     */
    public int getMicro() {
        return micro;
    }

    /**
     * Updates the version numbers.
     *
     * @param maj
     *            the new major value
     * @param min
     *            the new minor value
     * @param mic
     *            the new micro value
     */
    public void updateVersion(int maj, int min, int mic) {
        this.major = maj;
        this.minor = min;
        this.micro = mic;
    }

    // org.osgi.framework.Version
    /**
     * Creates a version given the specific String.
     *
     * @param version
     *                the string to parse
     * @return the version value corresponding to the String
     */
    public static Version parseVersion(String version) {
        if (version == null || version.trim().length() == 0) {
            return EMPTY_VERSION;
        }
        return new Version(version.trim());
    }

    /**
     * Returns the string corresponding to the version.
     */
    @Override
    public String toString() {
        return major + SEPARATOR + minor + SEPARATOR + micro;
    }
}
