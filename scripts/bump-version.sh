#!/bin/bash

echo "Enter new version :" 
read version
mvn org.eclipse.tycho:tycho-versions-plugin:set-version -DnewVersion=$version



echo "Commit and create tag [Y|N] :"
read createTag

if [ $createTag == "Y" ]; then
    git add "pom.xml"
    git add "**/pom.xml"
    git add "**/MANIFEST.MF"
    git add "**/feature.xml"
    git commit -m "Bump version ${version}"
    git tag -a v${version} -m "Version ${version}"
fi
